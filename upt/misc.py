"""Misc utility methods."""
from enum import Enum

from bs4 import BeautifulSoup as BS

from upt import const
from upt.logger import LOGGER


class Monotonic:
    """Get monotonic number."""

    def __init__(self, start_i=0):
        """Create an object."""
        self.__i = start_i

    def get(self):
        """Increase counter and return it."""
        self.__i += 1
        return self.__i


class RET(Enum):
    """Wraps all program return codes."""

    # Kernel testing passed.
    PASSED_TESTING = 0

    # Kernel failed testing.
    FAILED_TESTING = 1

    # Tests failed because of infrastructure issues
    INFRASTRUCTURE_ISSUE = 2

    # Resource (no longer?) ready infrastructure issue.
    # This may happen when a resource (e.g. Beaker job) is canceled during
    # test running. For whatever reason, we've failed a heartbeat check on
    # the resource and cannot continue as a result.
    RESOURCE_IS_DEAD = 3

    # Specific infrastructure issue.
    # For whatever reason, we've failed to provision a resource.
    # Maybe we've run out of time to finish provisioning. Or maybe there were
    # too many failures while doing that.
    PROVISIONING_FAILED = 4

    # Too many errors, please carefully debug the pipeline.
    TOO_MANY_ERRORS = 5

    # Bad command line arguments.
    CMD_LINE_ARGS = 6

    # Run aborted by user (signaled?)
    RUN_USER_ABORTED = 7

    # Restraint error or possibly an issue with code or data
    RESTRAINT_PROBLEM = 8

    @classmethod
    def merge_retcodes(cls, retcodes):
        """Combine multiple provisioner retcodes into one.

        The most severe retcode from the list will be returned, such that
        a passing retcode is only returned if all retcodes are passing.
        """
        return max(retcodes, key=lambda x: x.value, default=cls.PASSED_TESTING)


def recipe_not_provisioned(recipe):
    """Return status if recipe aborted and cannot run tasks."""
    status = recipe.get('status')
    return status if status in ('Aborted', 'Cancelled') else False


def recipe_installing_or_waiting(recipe):
    """Return True if recipe is installing."""
    return recipe.get('status') in ('Installing', 'Waiting')


def reservesys_task_problem(tasks):
    """Return cause if any task in tasks list aborted, cancelled or failed."""
    for task in tasks:
        if task['status'] in ('Aborted', 'Cancelled'):
            return task['status']
        if task['result'] == 'Fail':
            return task['result']

    return False


def fixup_or_delete_tasks_without_fetch(soup):
    """Delete <task /> elements that do not have <fetch /> in <params />."""
    for task in soup.find_all('task'):
        if not task.find_all('fetch'):
            if task['name'] == '/distribution/command':
                url = const.BKR_CORE_TASKS_URL.format(task='command')

                task.append(BS(f'<fetch url="{url}" />', 'xml').find('fetch'))

            else:
                # Delete task
                LOGGER.warning('%s removed from XML, no fetch element!', task["name"])
                task.decompose()


def compute_tasks_duration(tasks):
    """Add-up KILLTIMEOVERRIDE of all tasks in a list."""
    tasks_duration = 0
    for task in tasks:
        for param in task.findAll('param'):
            if param['name'] == 'KILLTIMEOVERRIDE':
                value = param['value']
                try:
                    tasks_duration += int(value)
                except ValueError:
                    LOGGER.debug('task duration was invalid: %s', value)

                break

    return tasks_duration


def compute_recipe_duration(recipe):
    """Add-up KILLTIMEOVERRIDE of all tasks in a recipe."""
    return compute_tasks_duration(recipe.findAll('task'))


def get_whiteboard(text, i, is_preprovisioned, **kwargs):
    """Optionally override whiteboard, so reporter query does not match."""
    dev_mode = kwargs['dev_mode']

    if dev_mode:
        text = text.replace('cki@gitlab', 'UPT@gitlab')

    if i is not None:
        text = text.replace('</whiteboard>', f' #{i:02d}</whiteboard>')

    if is_preprovisioned:
        text = text.replace('</whiteboard>', ' PREPROVISIONED</whiteboard>')

    return text


def is_task_waived(task):
    """Check if a Beaker task XML has CKI_WAIVED param that is set to true."""
    try:
        params = task.find_all('param')
    except AttributeError:
        return False

    for param in params:
        try:
            if param['name'] == 'CKI_WAIVED' and str(param['value']).lower() \
                    == 'true':
                return True
        except KeyError:
            continue

    return False
