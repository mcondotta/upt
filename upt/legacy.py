"""Legacy commands."""
import os
import sys

import click
from bs4 import BeautifulSoup as BS
from ruamel.yaml.scalarstring import PreservedScalarString

from cki_lib.logger import logger_add_fhandler

from provisioners.format import ProvisionData
from provisioners.objects import ResourceGroup, RecipeSet, Host

from upt import const
from upt import misc
from upt.glue import ProvisionerGlue
from upt.logger import LOGGER


def init_resource_group(xml, i, **kwargs):
    """Create a new resource group for each recipeSet.

    Restraint client has a nasty habit of exiting on EWD, even though that affects only 1 recipeSet, not the whole job.
    Because of this, each resource_group currently has only 1 recipeSet.
    """
    resource_group = ResourceGroup()
    soup = BS(xml, 'xml')
    job = soup.find('job')
    try:
        job['group'] = kwargs['group']
    except KeyError:
        pass

    # make job soup empty to create a template
    for child in job.find_all("recipeSet"):
        child.decompose()

    # save this in yaml in pretty format
    job_content = str(job).replace('\n', '').strip()
    job_content = misc.get_whiteboard(job_content, i, is_preprovisioned=False, **kwargs)
    resource_group.job = PreservedScalarString(job_content)

    return resource_group


def _delete_aborted_recipes(soup):
    """Remove aborted recipes/recipesets from soup."""
    for recipe in soup.find_all('recipe'):
        if misc.recipe_not_provisioned(recipe):
            recipe.decompose()

    for recipeset in soup.find_all('recipeSet'):
        if not recipeset.find_all('recipe'):
            recipeset.decompose()


def _delete_excluded_tests(soup, fname_excluded):
    """Delete tasks from soup that have names listed in fname_excluded."""
    if fname_excluded:
        with open(fname_excluded) as fhandle_excl:
            names = fhandle_excl.read().splitlines()
            for task in soup.find_all('task'):
                task_name = task['name']
                if task_name in names:
                    print(f'Exclude file: removing task {task_name} from '
                          'provisioning/test running')
                    task.decompose()


def convert_xml(xml, **kwargs):
    # pylint: disable=too-many-locals
    """Convert xml input file into Beaker legacy provisioning request."""
    # Create main data object, this will serialize into converted provisioning
    # request (c_req.yaml).
    prov = ProvisionData()

    # note down that this is a converted job
    bkr = prov.beaker

    # create the soup a-new, because the previous was decomposed
    soup = BS(xml, 'xml')
    _delete_excluded_tests(soup, kwargs['exclude'])
    _delete_aborted_recipes(soup)

    i = 1
    # Go through all recipesets in original XML and split them to resource groups.
    for recipeset in soup.find_all('recipeSet'):
        resource_group = init_resource_group(xml, i, **kwargs)
        bkr.rgs.append(resource_group)

        alt_soup = BS(const.JOB_XML_STUB, 'xml')
        restraint_soup = alt_soup.find('recipeSet')

        new_recipeset = RecipeSet()

        for recipe in recipeset.find_all('recipe'):
            new_recipe = BS('<recipe />', 'xml').find('recipe')
            for task in recipe.find_all('task'):
                new_recipe.append(task)

            restraint_soup.append(new_recipe)

            new_host = Host({'hostname': '', 'recipe_id': None, 'recipe_fill': ''})
            # Duration force by cmd-line args takes precendece. Otherwise use
            # 120% of added-up task KILLTIMEOVERRIDE. If that's still 0, use
            # 8hours.
            host_tasks_duration = misc.compute_recipe_duration(new_recipe)
            force_host_duration = kwargs['force_host_duration']
            new_host.duration = force_host_duration if force_host_duration \
                else int(host_tasks_duration / 100 * 120 if host_tasks_duration
                         else const.DEFAULT_RESERVESYS_DURATION)

            # override recipe_fill
            new_host.recipe_fill = PreservedScalarString(
                BS(str(recipe), 'xml').find('recipe'))
            new_recipeset.beaker_hosts.append(new_host)

        # remove and warn about tasks without fetch element; restraint cannot
        # run these
        misc.fixup_or_delete_tasks_without_fetch(alt_soup)
        # convert: write restraint xml
        new_recipeset.restraint_xml = PreservedScalarString(
            alt_soup.prettify())
        resource_group.recipeset = new_recipeset

        i += 1

    # Make sure we don't run on certain hosts.
    if kwargs.get('excluded_hosts'):
        prov.beaker.exclude_hosts(kwargs['excluded_hosts'])

    return prov


@click.group()
@click.pass_context
def legacy(ctx, **kwargs):
    """Commands for legacy pipeline."""
    # Put log file info.log into current directory.
    logger_add_fhandler(LOGGER, 'info.log', os.getcwd())


@legacy.command()
@click.option('-i', '--input_path', required=True,
              help='Path to beaker.xml to convert. Default: beaker.xml',
              default='beaker.xml')
@click.option('-r', '--request-file', required=True, default='c_req.yaml',
              help='Path to output (converted) file. Default: c_req.yaml.')
@click.option('-g', '--group', required=False, default=None,
              help='Optional. Beaker group override. When running using files'
                   ' from the pipeline (cki group), use "" to override group'
                   ' and submit jobs using your own account.')
@click.option('-e', '--exclude', required=False, default=None,
              help='A filename with names of excluded tests. These will be'
                   'removed from provisioning.')
@click.pass_context
def convert(ctx, **kwargs):
    """Convert standard kpet beaker.xml to UPT provisioning request."""
    # pack all arguments into kwargs
    kwargs.update(ctx.parent.parent.params)

    print(f'Converting {kwargs["input_path"]} to {kwargs["request_file"]}...')

    with open(kwargs['input_path']) as fhandle:
        # read xml
        provisioner_data = convert_xml(fhandle.read(), **kwargs)

    # write everything to file
    provisioner_data.serialize2file(kwargs['request_file'])


@legacy.command()
@click.option('-i', '--input_path', required=True,
              help='Path to beaker.xml to convert. Default: beaker.xml.',
              default='beaker.xml')
@click.option('-r', '--request-file', required=True, default='c_req.yaml',
              help='path to output (converted) file')
@click.option('-g', '--group', required=False, default=None,
              help='Optional. Beaker group override. When running using files'
                   ' from the pipeline (cki group), use "" to override group'
                   ' and submit jobs using your own account.')
@click.option('-e', '--exclude', required=False, default=None,
              help='A filename with names of excluded tests. These will be'
                   'removed from provisioning.')
@click.pass_context
def provision(ctx, **kwargs):
    # pylint: disable=too-many-arguments
    """Legacy Beaker provisioning by converting beaker.xml file."""
    print('Provisioning resources (legacy mode)...')
    # pack all arguments into kwargs
    kwargs.update(ctx.parent.parent.params)

    with open(kwargs['input_path']) as fhandle:
        # read xml
        # convert input specified by command line params to
        # Beaker legacy provision provisioning request
        provisioner_data = convert_xml(fhandle.read(), **kwargs)

    glue = ProvisionerGlue(provisioner_data, **kwargs)
    # write everything to file
    provisioner_data.serialize2file(kwargs['request_file'])

    # run provisioners; because of the data we've crafted,
    # only Beaker provisioning will run
    retcode = glue.run_provisioners()

    sys.exit(retcode.value)
