"""Output loggers."""
from cki_lib.logger import file_logger

LOGGER = file_logger(__name__, dst_file='info.log')
