"""Provisioner interface."""

from itertools import chain
import time
from abc import ABCMeta
from abc import abstractmethod

from typing import List

from upt.misc import RET
from provisioners.objects import ResourceGroup, RecipeSet
from provisioners.end_conditions import ProvEndConds


# pylint: disable=protected-access


class ProvisionerCore(metaclass=ABCMeta):
    """Provisioner interface."""

    rgs: List[ResourceGroup]

    def update_provisioning_state(self, force_recheck=False):
        """Update data for the purposes of tracking resources state."""
        for i, resource_group in enumerate(self.rgs, 1):
            if resource_group._provisioning_done and not force_recheck:
                # If the resource group is fully provisioned, then no errors
                # occurred and there's nothing to do.
                continue

            # Get provisioning state from data
            if self.is_provisioned(resource_group):
                print(f'{"Provisioning confirmed" if force_recheck else "Provisioning done"} for resource_group #{i}')
                # We can leave this resource group alone now.
                resource_group._provisioning_done = True

    @abstractmethod
    def provision(self, **kwargs):
        """Pre-provision extra hosts."""

    @abstractmethod
    def set_reservation_duration(self, host, strict_keycheck='no'):
        """Ensure host remains provisioned for host.duration."""

    @abstractmethod
    def is_provisioned(self, resource_group):
        """Check if resource group is finished provisioning."""

    @abstractmethod
    def reprovision_aborted(self):
        """Provision a resource again, if provisioning failed."""

    def wait(self, resource_list, **kwargs):
        """Wait for resource provisioned by provision() to be ready."""
        print(f'Waiting for {" ".join(resource_list)} to be ready...')

        # Helper object to determine if provisioning is finished.
        # Re-provisions resources and check their state. Will return a retcode
        # at the end of execution of wait().
        end_conditions = ProvEndConds(self.rgs, self.update_provisioning_state,
                                      self.reprovision_aborted)
        while not end_conditions.evaluate(False, **kwargs):
            # wait a while before checking on the resource state again
            time.sleep(60)

        time.sleep(120)
        force_recheck = True
        while not end_conditions.evaluate(force_recheck, **kwargs):
            force_recheck = False
            time.sleep(60)

        if end_conditions.retcode == RET.PROVISIONING_FAILED:
            print('* Returning resources.')
            # Release resources we've provisioned in this run.
            self.release_resources()
        elif end_conditions.retcode == RET.PASSED_TESTING:
            print('Resource(s) provisioned.')

        return end_conditions.retcode

    @abstractmethod
    def release_resources(self):
        """Release resources of this script run (e.g. cancel Beaker job)."""

    @abstractmethod
    def get_resource_ids(self):
        """Return identifiers of resources provisioned during script run."""

    @abstractmethod
    def heartbeat(self, resource_group, recipe_ids_dead, ewd_recipe_ids_tasks):
        """Check if resource is OK (provisioned, not broken/aborted)."""

    @abstractmethod
    def update_provisioning_request(self):
        """Ensure that request file has up-to-date info after provisioning.

        This method is to be called at the end, when we know we've successfully
        provisioned resources we need. This method will skip resources that
        failed provisioning.
        """

    def find_host_object(self, recipe_id):
        """Find host by recipe_id."""
        result = self.find_objects(self.rgs, lambda obj: obj if hasattr(obj, 'recipe_id') else None,
                                   filter_hosts=lambda host: host.recipe_id == recipe_id)

        assert len(result) == 1, "Invalid number of results"

        return result[0]

    @classmethod
    def all_recipes_finished(cls, resource_groups):
        """Determine if all hosts are finished processing."""
        for host in cls.find_objects(resource_groups, lambda obj: obj if hasattr(obj, 'recipe_id') else None):
            if not host._done_processing:
                return False

        return True

    @classmethod
    def get_all_hosts(cls, resource_groups):
        """Return all hosts of resource_groups."""
        return list(chain(*cls.find_objects(resource_groups, lambda x: x.hosts if isinstance(x, RecipeSet) else None)))

    @staticmethod
    def single_filter(lst, filter_obj=None):
        """Return a list filtered using filter_obj or the full list."""
        return filter(filter_obj, lst) if filter_obj else lst

    @staticmethod
    def conditional_append(getter_func, src_obj, dest_obj):
        """Append result of getter_func(src_obj) to dest_obj if the result is not None."""
        result_of_get = getter_func(src_obj)
        if result_of_get is not None:
            dest_obj.append(result_of_get)

    @classmethod
    def find_objects(cls, resource_groups, getter_func, filter_rgs=None, filter_recipesets=None, filter_hosts=None):
        # pylint: disable=too-many-arguments
        """Iterate objects filtered using respective filters and return results matched by getter_func.

        This is meant to retrieve objects or their attributes from resource_groups. Resource_groups/recipesets/hosts
        are filtered using respective filters and getter_func is called for each single of these objects. If it
        evaluates non-None, the result is appended to the final result list.

        Arguments:
            resource_groups    - list of all resource_groups to iterate through
            getter_func        - function that is called in each loop on (resource_group/recipeset/host), returns what
                                 should be appended to results
            filter_rgs         - filter object to use on resource_groups
            filter_recipesets  - filter object to use on recipesets
            filter_hosts       - filter object to use on hosts

        Returns: all objects getter_func returned
        """
        results = []

        # Iterate through pre-filtered resource_groups
        for resource_group in cls.single_filter(resource_groups, filter_rgs):
            # Conditionally append resource_group to results, if getter_func evaluates non-None
            cls.conditional_append(getter_func, resource_group, dest_obj=results)

            # Iterate through pre-filtered resource_groups
            for recipeset in cls.single_filter([resource_group.recipeset], filter_recipesets):
                # Conditionally append recipeset to results, if getter_func evaluates non-None
                cls.conditional_append(getter_func, recipeset, dest_obj=results)

                # Iterate through pre-filtered hosts
                for host in cls.single_filter(recipeset.hosts, filter_hosts):
                    # Conditionally append host to results, if getter_func evaluates non-None
                    cls.conditional_append(getter_func, host, dest_obj=results)

        return results
