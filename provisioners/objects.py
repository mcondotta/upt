"""Datastructure definition and validation for provisioning request."""

import itertools
from typing import List

from ruamel.yaml.scalarstring import PreservedScalarString

from provisioners.serializer import DeserializerBase
from upt.misc import RET


class Host(DeserializerBase):
    """Datastructure describing a host to provision."""

    duration: int

    # host common, can't use a base class because of serialization
    hostname: str
    recipe_id: int
    recipe_fill: PreservedScalarString

    _done_processing = False

    _retcode = RET.INFRASTRUCTURE_ISSUE
    _task_results = []
    _rerun_recipe_tasks_from_index = None

    misc: dict = {}
    # AWS Only
    _user_data_done = False
    _instance = None


class RecipeSet(DeserializerBase):
    """Datastructure describing a recipeset to provision in Beaker."""

    aws_hosts: List[Host]
    beaker_hosts: List[Host]
    restraint_xml: PreservedScalarString

    _attempts_made = 0
    _tests_finished = False
    _waiting_for_rerun = False

    @property
    def hosts(self):
        """Return list of all hosts for all types of provisioners."""
        ret = list(itertools.chain(self.aws_hosts, self.beaker_hosts))

        if len(ret) == 0:
            raise RuntimeError('no hosts found!')

        return ret


class ResourceGroup(DeserializerBase):
    """A group of resources provisioned together."""

    resource_id: str
    recipeset: RecipeSet
    job: str
    _provisioning_done: bool = False
    _erred_rset_ids: set = set()
    _reprovisioned_rsets_ids: set = set()
    _provisioned_ok_rsets_ids: set = set()
    preprovisioned: bool = False

    def aws_hosts(self):
        """Iterate through all AWS hosts."""
        for aws_host in self.recipeset.aws_hosts:
            yield aws_host
