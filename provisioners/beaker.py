"""Simple & extensible Beaker provisioner."""

import copy
import itertools
import pathlib
import re
import shlex
import subprocess
import time

from bs4 import BeautifulSoup as BS
from ruamel.yaml.scalarstring import PreservedScalarString

from cki_lib import misc
from cki_lib.retrying import retrying_on_exception

from upt import const
from upt.misc import recipe_not_provisioned
from upt.misc import reservesys_task_problem
from upt.misc import recipe_installing_or_waiting
from upt.misc import fixup_or_delete_tasks_without_fetch
from upt.misc import get_whiteboard
from upt.logger import LOGGER
from provisioners.objects import ResourceGroup, RecipeSet, Host
from provisioners.interface import ProvisionerCore
from provisioners.serializer import DeserializerBase

from restraint_wrap.task_result import TaskResult

WATCHDOG_EXTEND_SCRIPT = """PORT="$(journalctl -u restraintd | grep -Po "Listening on http.*?:([0-9]+)"
 |tail -1 |cut -f3 -d ":")";rstrnt-adjust-watchdog -s http://localhost:${{PORT}}/recipes/{recipe_id}/
watchdog {timespec}""".replace('\n', '')


class Beaker(DeserializerBase, ProvisionerCore):
    # pylint: disable=no-member,too-many-public-methods
    """Datastructure describing Beaker provisioner."""

    # a set of recipe_ids that aborted
    _warned_rsetids = set()

    # pylint: disable=protected-access

    def set_reservation_duration(self, host, strict_keycheck='no'):
        """Ensure host remains provisioned for host.duration."""
        # Found hostname, so we know recipe_id
        script = WATCHDOG_EXTEND_SCRIPT.format(recipe_id=host.recipe_id, timespec=host.duration).encode('utf-8')

        cmd = f'ssh root@{host.hostname} -o ' \
              f'StrictHostKeyChecking={strict_keycheck} "bash -s"'
        stdout, err, retcode = misc.safe_popen(shlex.split(cmd),
                                               stdin_data=script,
                                               stdin=subprocess.PIPE,
                                               stderr=subprocess.PIPE,
                                               stdout=subprocess.PIPE)
        if err:
            LOGGER.debug('set_reservation_duration error: %s', err)

        if retcode:
            raise RuntimeError(f'cannot adjust watchdog of {host.hostname}')

        if stdout.strip():
            LOGGER.info('set_reservation_duration: %s', stdout.strip())

    @classmethod
    @retrying_on_exception(RuntimeError)
    def get_bkr_results(cls, resource_id):
        """Get Beaker job results for resource_id."""
        cmd = f'bkr job-results {resource_id}'

        stdout, _, returncode = misc.safe_popen(shlex.split(cmd),
                                                stdout=subprocess.PIPE)
        if returncode:
            print(stdout)
            raise RuntimeError('bkr job-results failed!')

        return stdout

    @classmethod
    def get_dead_resource_info(cls, result_recipes, recipe_ids, ewd_recipe_ids_tasks):
        """Try to determine what caused the resource to become unresponsive."""
        # Get recipes (XML) for recipe_id's that we know have some sort of issue.
        ewd_recipe_ids = [x[0] for x in ewd_recipe_ids_tasks]
        # Exclude ewds that are already reported.
        recipes = list(filter(lambda x: int(x['id']) in recipe_ids and int(x['id']) not in ewd_recipe_ids,
                              result_recipes))
        for recipe in recipes:
            # Construct TaskResult objects for all tasks
            task_results = TaskResult.task2task_results(int(recipe['id']), recipe.findAll('task'))
            # Select reservesys tasks only
            reservesys_task_results = list(filter(lambda x: 'reservesys' in x.testname, task_results))

            # Look for explicit EWD description
            for task_result in reservesys_task_results:
                for result in task_result.task.findAll('result'):
                    if result.text and result.text == 'External Watchdog Expired':
                        print(f'#{task_result.recipe_id}: found EWD in reservesys task!')
                        # Add recipe_id and task_result to a set of objects we know received EWD.
                        ewd_recipe_ids_tasks.add((int(recipe['id']), task_result))

    def quirk_wait(self, resource_group):
        """Wait a while, then get Beaker results again."""
        delay = 60
        print(f'* Caught Beaker quirk; waiting {delay}...')
        time.sleep(delay)

        _, result_recipesets, erred_rset_ids = self.get_provisioning_state(resource_group)
        return result_recipesets, erred_rset_ids

    @classmethod
    def check_beaker_quirk(cls, erred_recipes):
        """Check if Beaker quirk occured."""
        for erred_rec in erred_recipes:
            if erred_rec['status'] == 'Aborted' and \
                    erred_rec['result'] == 'New':
                return True
            for task in erred_rec.findAll('task'):
                if task['status'] == 'Aborted' and \
                        task['result'] == 'New':
                    return True

        return False

    @staticmethod
    def extract_ids(result_recipesets, erred_rset_ids):
        """Return list of ids of recipes and recipesets."""
        # all recipes of recipesets
        result_recipes = list(itertools.chain(*[recipeset.findAll('recipe') for recipeset in result_recipesets]))
        # recipesets that had an issue
        erred_rsets = [recipeset for recipeset in result_recipesets if int(recipeset['id']) in erred_rset_ids]
        # recipe_ids that had an issue
        erred_recipe_ids = [int(recipe['id']) for recipe in itertools.chain(*[recipeset.findAll('recipe') for
                                                                              recipeset in erred_rsets])]
        # recipes that had an issue
        erred_recipes = [recipe for recipe in result_recipes if int(recipe['id']) in erred_recipe_ids]

        return result_recipes, erred_recipe_ids, erred_recipes

    def heartbeat(self, resource_group, recipe_ids_dead, ewd_recipe_ids_tasks):
        """Check if resource is OK (provisioned, not broken/aborted)."""
        # Check resources in resource_group only
        _, result_recipesets, erred_rset_ids = self.get_provisioning_state(resource_group)
        if not erred_rset_ids:
            # All good, continue with next resource_group
            return

        result_recipes, erred_recipe_ids, erred_recipes = self.extract_ids(result_recipesets,
                                                                           erred_rset_ids)
        # Beaker quirk: transition of result: New -> Panic. Redownload data after 60 seconds.
        while self.check_beaker_quirk(erred_recipes):
            result_recipesets, erred_rset_ids = self.quirk_wait(resource_group)
            result_recipes, erred_recipe_ids, erred_recipes = self.extract_ids(result_recipesets,
                                                                               erred_rset_ids)

        # Get recipe_ids that have failed and save hostnames that belong
        # to them.
        self.get_dead_resource_info(result_recipes, erred_recipe_ids, ewd_recipe_ids_tasks)
        for host in resource_group.recipeset.beaker_hosts:
            # We're rather careful here. There shouldn't be any extra Host objects, so it's safest to check
            # against those, because these are being used to run restraint client.
            if host.recipe_id in erred_recipe_ids:
                recipe_ids_dead.add(host.recipe_id)

    def warn_once_provisioning_issue(self, recipe_id, recipe_ids, rset_id, err_status, reservesys_err_status):
        # pylint: disable=too-many-arguments
        """Warn once about an issue with rset_id provisioning; print all recipeSet ids and all recipe ids."""
        if rset_id not in self._warned_rsetids:
            if err_status:
                msg = f'* RS:{rset_id} ({recipe_ids}): R:{recipe_id} {err_status}.'
            elif reservesys_err_status:
                msg = f'* RS:{rset_id} ({recipe_ids}): found reservesys task that {reservesys_err_status}.'
            else:
                raise RuntimeError('unhandled condition; cannot determine what caused recipe to abort')

            print(msg)
            self._warned_rsetids.add(rset_id)

    def is_provisioned(self, resource_group):
        """Check if resource group is finished provisioning."""
        provisioned, _, _ = self.get_provisioning_state(resource_group)
        return provisioned

    def get_provisioning_state(self, resource_group):
        # pylint: disable=no-else-break
        """Get info about provisioning.

        This checks every recipeSet in resource_group. The method pulls fresh data from Beaker.
        If any recipe of a recipeSet has an issue, or doesn't have reservesys task in a proper state, then
        we add recipeSet id to the set of 'erred' recipeSets. This ends-up ensuring that entire recipeSet is cloned,
        we don't assume bigger granularity. If that's required, then it's a bad decision on the part of who provides
        us the Beaker job xml; we have to respect synchronization domains and this is the simplest way.

        NOTE: this method is not a required part of the provisioner interface. Each provisioner implements and uses
        this method in its own way, although the method signature is very similar each time.
        """
        soup = BS(self.get_bkr_results(resource_group.resource_id), 'xml')

        recipesets = soup.findAll('recipeSet')
        for recipeset in recipesets:
            recipes = recipeset.find_all('recipe')
            # Start with no issues for this recipeSet.
            any_error = False
            rset_installing = False
            for recipe in recipes:
                # get all tasks restraint uses to reserve system
                tasks = recipe.findAll('task', {'name': '/distribution/reservesys'})

                # scan for abort in recipe status
                err_status = recipe_not_provisioned(recipe)
                # scan for abort in reservesys task
                reservesys_err_status = reservesys_task_problem(tasks)
                any_error = err_status or reservesys_err_status
                rset_installing |= recipe_installing_or_waiting(recipe)

                if any_error:
                    self.warn_once_provisioning_issue(recipe['id'], [rec['id'] for rec in recipes], recipeset['id'],
                                                      err_status, reservesys_err_status)
                    # Add any recipeSet id that has issue. We will have to clone it. Don't check other recipes,
                    # there's no point.
                    resource_group._erred_rset_ids.add(int(recipeset['id']))
                    break
                else:
                    for task in tasks:
                        # We're iterating through tasks again. If the recipe is really in a good state, then it should
                        # have reservesys task with Running/New status/result.
                        if task['result'] != 'New' or task['status'] != 'Running':
                            LOGGER.debug('%i: %s %s %s', int(recipe['id']), task['name'], task['status'],
                                         task['result'])
                            any_error = True
                            break

            if not any_error and not rset_installing:
                # Add recipe that provisioned OK. The recipe has to belong to this rg.
                resource_group._provisioned_ok_rsets_ids.add(int(recipeset['id']))

        # This is the ultimate condition to checking if everything was provisioned/reprovisioned.
        provisioned = len(recipesets) == len(resource_group._reprovisioned_rsets_ids) + \
            len(resource_group._provisioned_ok_rsets_ids)

        return provisioned, recipesets, resource_group._erred_rset_ids

    @staticmethod
    def delete_failed_hosts(recipeset):
        """Remove hosts that failed provisioning."""
        recipeset.beaker_hosts = list(filter(lambda host: host.recipe_id is not None and host.hostname != '',
                                             recipeset.beaker_hosts))

        # Delete recipes in restraint_xml that failed to provision
        soup = BS(recipeset.restraint_xml, 'xml')
        for recipe in soup.findAll('recipe'):
            if not recipe.get('id'):
                recipe.decompose()
        recipeset.restraint_xml = PreservedScalarString(soup.prettify())

    def update_provisioning_request(self):
        """Ensure that request file has up-to-date info from Beaker.

        Resources that were provisioned OK must have accurate recipe_id and hostname; the coresponding restraint_xml
        has to be updated.
        Tasks without fetch element have to be deleted, because restraint cannot run those.
        """
        for resource_group in list(filter(lambda rg: not rg.preprovisioned, self.rgs)):
            soup = BS(self.get_bkr_results(resource_group.resource_id), 'xml')

            recipeset, xml_recipeset = resource_group.recipeset, soup.findAll('recipeSet')[0]
            for i, (recipe, host) in enumerate(zip(xml_recipeset.findAll('recipe'), recipeset.beaker_hosts)):
                # scan for abort in recipe status
                err_status = recipe_not_provisioned(recipe)
                # scan for abort in reservesys task
                reservesys_err_status = reservesys_task_problem(recipe.findAll('task'))
                any_error = err_status or reservesys_err_status
                system = recipe.get('system')
                if not any_error and system:
                    host.hostname = system
                    host.recipe_id = int(recipe['id'])

                    # update restraint xml
                    restraint_soup = BS(recipeset.restraint_xml, 'xml')

                    # Non-preprovisioned hosts: set recipe_id to restraint_xml
                    recipe2mod = restraint_soup.findAll('recipe')[i]
                    recipe2mod['id'] = int(recipe['id'])

                    fixup_or_delete_tasks_without_fetch(restraint_soup)

                    recipeset.restraint_xml = PreservedScalarString(restraint_soup.prettify())
                else:
                    host.recipe_id = None
                    host.hostname = ''

                    if not resource_group._erred_rset_ids:
                        LOGGER.warning('Error: a system went down shortly after provisioning was completed!')

            self.delete_failed_hosts(recipeset)

            self.delete_empty_resource_groups()

    def delete_empty_resource_groups(self):
        """Delete resource groups that have no hosts."""
        self.rgs = [resource_group for resource_group in self.rgs if any(resource_group.recipeset.beaker_hosts)]

    def resource_group2xml(self, resource_group):
        """Read provisioner data resource group, create XML."""
        soup = BS(resource_group.job, 'xml')
        job = soup.find('job')

        # reconstruct XML from legacy provisioning request
        new_recipeset = self._finalize_recipeset(resource_group.recipeset)
        job.append(BS(new_recipeset, 'xml').find('recipeSet'))

        return self._finalize_recipes(soup, resource_group.recipeset)

    def preprovision(self, **kwargs):
        """Pre-provision extra hosts."""
        preprovision_arg = kwargs.get('preprovision')
        preprovision_indexes = [int(x) for x in preprovision_arg.split(',')] if preprovision_arg else []

        new_resource_groups = []
        new_xmls = []
        if not preprovision_indexes:
            LOGGER.debug('* No pre-provisioning enabled.')
            return new_xmls, new_resource_groups

        for i, resource_group in enumerate(self.rgs):
            if i in preprovision_indexes:
                # Make a copy
                new_rg = copy.deepcopy(resource_group)
                for host_copy in self.find_objects([new_rg], lambda x: x if isinstance(x, Host) else None):
                    # We don't actually know the reserve time of pre-provisioned hosts, because we don't yet know what
                    # tasks we will run.
                    host_copy.duration = const.DEFAULT_RESERVESYS_DURATION
                    # We don't know hostname or recipe_id yet, ensure it's empty.
                    host_copy.hostname, host_copy.recipe_id = '', None
                    # NOTE: recipe_fill is kept as-is to provide the same conditions. We have to trust user/script is
                    # smart enough not to preprovision resource groups with forced hostnames.

                for cpy_recipset in self.find_objects([new_rg], lambda x: x if isinstance(x, RecipeSet) else None):
                    # We don't know what tasks we'll run, so this is empty.
                    cpy_recipset.restraint_xml = PreservedScalarString('')

                # Make sure this isn't set; we can't have resource_id yet, nothing was submitted
                new_rg.resource_id = ''

                # save this in yaml in pretty format
                job_content = str(new_rg.job).replace('\n', '').strip()
                job_content = get_whiteboard(job_content, i=None, is_preprovisioned=True, **kwargs)
                new_rg.job = PreservedScalarString(job_content)

                # Mark this as a special, pre-provisioned resource group, so we know how to get this sort of resource.
                new_rg.preprovisioned = True

                # Add XML and resource group to list
                new_xmls.append(self.resource_group2xml(new_rg))
                new_resource_groups.append(new_rg)

        assert len(new_xmls) == len(new_resource_groups), "Broken code, counts of xmls and resource groups must match"

        return new_xmls, new_resource_groups

    def provision(self, **kwargs):
        """Entry point, provision resources according to provisioner_data."""
        xmls = [self.resource_group2xml(rg) for rg in self.rgs]
        new_xmls, new_resource_groups = self.preprovision(**kwargs)
        if new_xmls and new_resource_groups:
            self.rgs += new_resource_groups
            xmls += new_xmls

        all_resource_ids = []
        for i, xml in enumerate(xmls):
            # submit the job
            with misc.tempfile_from_string(xml.encode('utf-8')) as fname:
                # we submit by 1 resource_id (1 Beaker job)
                resource_id = self.submit_job(fname)[0]
                # save the resource id, so we can return it
                all_resource_ids.append(resource_id)
                # save resource ids of what we've provisioned
                self.rgs[i].resource_id = resource_id

        return all_resource_ids

    @classmethod
    def set_harness(cls, soup):
        """Override ks_meta in a recipe to restraint."""
        # <recipe ks_meta="harness='restraint-rhts beakerlib'" >
        restraint = "harness='restraint-rhts beakerlib'"
        for recipe in soup.find_all('recipe'):
            ks_meta = recipe.get('ks_meta', '')
            match_result = re.match('.*?(harness=[\'\"](.*?)[\'\"]).*?', str(ks_meta))
            if ks_meta and match_result:
                # Replace harness
                current_harness = match_result.group(1)
                recipe['ks_meta'] = ks_meta.replace(current_harness, restraint)
            elif ks_meta and not match_result:
                # Append harness
                recipe['ks_meta'] = f'{ks_meta} {restraint}'
            else:
                # Set harness
                recipe['ks_meta'] = restraint

    @classmethod
    def adjust_recipes(cls, soup, recipeset):
        """Add reservesys tasks with duration, remove dummy command."""
        hosts_durations = []

        for host in recipeset.beaker_hosts:
            hosts_durations.append(host.duration)

        for j, recipe in enumerate(soup.findAll('recipe')):
            duration = hosts_durations[j] if hosts_durations[j] else \
                const.DEFAULT_RESERVESYS_DURATION
            # Log this, could be important.
            LOGGER.debug('recipe duration is: %i', duration)

            reservesys = f"""<task name="/distribution/reservesys" role="None">
            <fetch url="{const.URL_RESERVESYS_TASK}"/><params>
            <param name="RESERVETIME" value="{duration}" /><params /></task>"""
            # add reservesys task
            recipe.append(BS(reservesys, 'xml'))

        return soup

    @classmethod
    def submit_job(cls, fname, dryrun=False):
        """Submit a job to Beaker."""
        dryrun_opt = '--dryrun' if dryrun else ''
        cmd = f'bkr job-submit {dryrun_opt} {fname}'

        stdout, _, returncode = misc.safe_popen(shlex.split(cmd),
                                                stdout=subprocess.PIPE)
        if returncode:
            print(stdout)
            raise RuntimeError('submitting job failed!')

        # process stdout using a regex to find J:XXXX resource ids
        resource_ids = re.findall(const.PATTERN_JOBIDS, stdout)

        # let user know what was submitted
        print(f'Submitted: {" ".join(resource_ids)}')

        return resource_ids

    @staticmethod
    def clone(task_spec):
        """Re-provision a task_spec like J: or RS:."""
        cmd = f'bkr job-clone {task_spec}'

        stdout, _, returncode = misc.safe_popen(shlex.split(cmd),
                                                stdout=subprocess.PIPE)
        if returncode:
            print(stdout)
            raise RuntimeError(f'cloning {task_spec} failed!')

        # process stdout using a regex to find J:XXXX resource ids
        resource_ids = re.findall(const.PATTERN_JOBIDS, stdout)

        return resource_ids

    @staticmethod
    def cancel(task_spec):
        """Cancel a task_spec like J: or RS:."""
        cmd = f'bkr job-cancel {task_spec}'

        stdout, _, returncode = misc.safe_popen(shlex.split(cmd),
                                                stdout=subprocess.PIPE)
        print(stdout.strip())
        if returncode:
            raise RuntimeError(f'cancelling {task_spec} failed!')

    def reprovision_aborted(self):
        # pylint: disable=cell-var-from-loop
        """Provision a resource again, if provisioning failed.

        This is a Beaker method that looks at provisioner resources per-recipe.
        Those recipes that failed provisioning (canceled/aborted) are cloned
        and the recipe_id is added to a list of recipe_ids that are not going
        to be cloned again. If we need to, then we clone a clone of the
        original recipe_id.
        """
        for resource_group in self.rgs:
            # Get all recipeSets that had any issue (aborted, ...) and were not reprovisioned
            not_reprovisioned_rset_ids = set(filter(lambda rset_id: rset_id not in
                                                    resource_group._reprovisioned_rsets_ids,
                                                    resource_group._erred_rset_ids))
            if not not_reprovisioned_rset_ids:
                # Skip resource group if there's nothing to reprovision.
                continue

            # Cancel recipeSets, we have to provision everything again!
            for rset_id in not_reprovisioned_rset_ids:
                self.cancel(f'RS:{rset_id}')

            # Mark all recipeSets as cloned. Don't clone the original recipeSets again, but keep it as info about what
            # failed provisioning.
            resource_group._reprovisioned_rsets_ids = resource_group._reprovisioned_rsets_ids.union(
                not_reprovisioned_rset_ids)

            for rset_id in not_reprovisioned_rset_ids:
                # create a new resource group to contain the clone recipes
                new_rg = ResourceGroup()
                new_rg.recipeset = copy.deepcopy(resource_group.recipeset)
                # Save resource_id of what we've just submitted!
                new_rg.resource_id = self.clone(f'RS:{rset_id}')[0]
                # let user know what's happening
                print(f'* Cloned RS:{rset_id} as {new_rg.resource_id}')
                # copy-over job attribute from our initial template
                new_rg.job = resource_group.job
                # append to provisioning request!
                self.rgs.append(new_rg)

    @classmethod
    def _finalize_recipeset(cls, recipeset):
        """Take recipeset template and create <recipeSet /> elements."""
        new_recipes = [host.recipe_fill for host in recipeset.beaker_hosts]
        new_recipeset = '<recipeSet >' + '\n'.join(new_recipes) + \
                        '</recipeSet>'

        return new_recipeset

    def _finalize_recipes(self, soup, recipeset):
        """Add restraint reservesys and set harness."""
        soup = self.adjust_recipes(soup, recipeset)

        # set harness to restraint
        self.set_harness(soup)

        return soup.prettify()

    def release_resources(self):
        """Release resources of this script run (cancel Beaker job)."""
        for resource_id in self.get_resource_ids():
            self.cancel(resource_id)

    def exclude_hosts(self, restrict_fname):
        """Ensure that we do not provision specific hosts."""
        for host in self.find_objects(self.rgs, lambda x: x if isinstance(x, Host) else None):
            soup = BS(host.recipe_fill, 'xml')

            for host_requires in soup.findAll('hostRequires'):
                if host_requires.get('force'):
                    # If we specifically want to run on a certain host, so be it.
                    continue

                and_node = host_requires.find('and')
                if and_node is None:
                    and_node = BS('<and />', 'xml').find('and')

                for restricted in pathlib.Path(restrict_fname).read_text().splitlines():
                    # Ensure hostname isn't appended twice.
                    if not and_node.find('hostname', attrs={'op': '!=', 'value': restricted}):
                        LOGGER.debug('Excluding hostname %s', restricted)
                        hostname_node = BS(f'<hostname op="!=" value="{restricted}" />', 'xml')
                        and_node.append(hostname_node)

                host_requires.append(and_node)

            host.recipe_fill = PreservedScalarString(soup.find('recipe').prettify())

    def get_resource_ids(self):
        """Return identifiers of resources provisioned during script run."""
        return [rg.resource_id for rg in self.rgs]
