"""Datastructure definition and validation for provisioner data."""

from cki_lib.yaml_wrap import RuamelSerializer

from provisioners.aws import AWS
from provisioners.beaker import Beaker
from provisioners.serializer import DeserializerBase


class ProvisionData(DeserializerBase):
    """Provisioner data interface."""

    aws: AWS
    beaker: Beaker

    def provisioners(self):
        """Return provisioners of all types as a single list.

        Filter out provisioners with no resource groups, which deserialization
        will create for all unused provisioner types.
        """
        ret = [provisioner for provisioner in
               [
                   self.aws,
                   self.beaker
               ] if len(provisioner.rgs) > 0]

        if len(ret) == 0:
            raise RuntimeError('no provisioner with implementation found!')

        return ret

    @classmethod
    def _register_classes(cls):
        """Register classes for yaml (de)serialization."""
        ruaml = RuamelSerializer()

        # Register subclasses of DeserializerBase, so ruaml knows what classes
        # to (de)serialize.
        for class_name in set(DeserializerBase.__subclasses__()):
            ruaml.yaml.register_class(class_name)

        return ruaml

    def serialize2file(self, fpath):
        """Serialize ProvisionData instance to a file using yaml."""
        with open(fpath, 'w') as fhandle:
            # make sure serializer knows what classes to use
            ruaml = self._register_classes()

            return ruaml.serialize(self, fhandle)

    @classmethod
    def deserialize_file(cls, path):
        """Deserialize a file into a ProvisionData instance."""
        # make sure deserializer knows what classes to use
        ruaml = cls._register_classes()

        result = ruaml.deserialize_file(path)
        return ProvisionData(result)
