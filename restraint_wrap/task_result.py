"""A result of a task run, with extra arguments to simplify working with it."""

from bs4 import BeautifulSoup as BS

from upt.misc import is_task_waived


class TaskResult:
    # pylint: disable=too-many-instance-attributes,too-many-arguments
    """A result of task that was run."""

    def __init__(self, recipe_id, task, task_id, result, status,
                 prev_task=None, host=None, **kwargs):
        """Create the object."""
        # The task result is identified by recipe_id
        self.recipe_id = int(recipe_id)
        self.task = task
        self.task_id = task_id
        self.result = result
        self.status = status
        self.prev_task = prev_task
        self.host = host
        self.kwargs = kwargs
        self.statusresult = status if not result else f'{status}: {result}'

        # Derived
        self.waived = is_task_waived(task)
        self.testname = task['name']
        self.prv_tsk_panicked_waived = prev_task.waived and prev_task.result \
            == 'Panic' if prev_task else False

        soup = BS(str(task), 'xml')
        param = soup.find('param', attrs={'name': 'CKI_ORIG_TASK_ID'})
        param = soup.find('param', attrs={'name': 'CKI_ORIG_RECIPE_ID'})
        self.orig_recipe_id = int(param['value']) if param else None

        self.subtask_results = []
        self.lwd_hit = False

    @classmethod
    def task2task_results(cls, recipe_id, tasks):
        """Convert tasks to task_results, without host or other params."""
        results = []
        for task in tasks:
            try:
                prev_task = results[-1].task
            except IndexError:
                prev_task = None

            new_task_result = TaskResult(recipe_id, task, task['id'],
                                         task['result'], task['status'],
                                         prev_task=prev_task)

            results.append(new_task_result)

        return results
