"""Actions to do on a specific test result."""
from upt.logger import LOGGER


class ActionOnResult:
    """Represent actions done on a specific task result."""

    # Example:
    #   Argument: lst_actions
    #   Value:    []
    #   Effect:   no methods run when conditions are satisfied
    #
    #   Argument: msg_string
    #   Value:    '(Just caught a specific status. Keep testing!)'
    #   Effect:   suffix of a message printed when conditions are satisfied
    #
    #   Argument: retcode
    #   Value:    None
    #   Effect:   Don't set any retcode when conditions are satisfied
    #
    #   Argument: kwargs
    #   Value:    status='Running'
    #   Effect:   Conditions are satisfied when status field is 'Running'
    #
    # ActionOnResult([],
    #                '(Just caught a specific status. Keep testing!)',
    #                None,
    #                status='Running'),
    def __init__(self, lst_actions, msg_string, retcode, **kwargs):
        """Create the object."""
        self.kwargs = kwargs
        self.lst_actions = lst_actions
        # A retcode that this action sets to its host
        self.retcode = retcode
        opt_retcode = f' -> {self.retcode}' if self.retcode is not None else ''
        self.msg_string = 'Recipe: #{recipe_id}: {statusresult}: ' \
                          '{testname}' + opt_retcode + '\n    ' + msg_string \
                          + '\n'

    def __str__(self):
        """Describe what conditions are set by kwargs for action to evaluate to True."""
        values = ' '.join([f'{arg}={self.kwargs[arg]}' for arg in self.kwargs])

        return values

    def eval(self, **kwargs):
        """Evaluate whether set conditions are fulfilled."""
        if not self.kwargs:
            # Don't match empty conditions as satisfied.
            return None

        for key in self.kwargs:
            # We're supposed to match a key that isn't specified in the
            # condition, so we don't know what the result is.
            if key != 'status' and key not in kwargs:
                LOGGER.error('offending key: %s', key)
                raise RuntimeError('invalid arguments')

        for arg in self.kwargs:
            requested_condition = self.kwargs[arg]
            if kwargs[arg] not in requested_condition:
                return None

        # the status entry matches all the conditions specified
        return True
