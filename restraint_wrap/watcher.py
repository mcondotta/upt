"""Watch restraint files for subtask results and LWD hits."""
import re
from threading import Semaphore
from queue import Queue

from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler

from upt.logger import LOGGER


class Watcher:
    """Watch file changes in a directory and print subtask results."""

    def __init__(self):
        """Create the object."""
        self.observer = Observer()
        self.sem_lwd_sync = Semaphore(1)
        self.event_handler = Handler(self.sem_lwd_sync)

    def run(self, directory2watch):
        """Start watching a directory."""
        LOGGER.info('* watching %s for subtask results', directory2watch)
        self.observer.schedule(self.event_handler, directory2watch, recursive=True)
        self.observer.start()
        self.observer.join()
        LOGGER.debug('* observer exiting')

    def __del__(self):
        """Destructor does cleanup."""
        # cleanup opened files
        for watched_file in self.event_handler.watched_files:
            try:
                watched_file.fhandle.close()
            except AttributeError:
                pass

    def get_subtask_results(self):
        """Get subtasks: (recipe_id, task_id, testname, result, score)."""
        subtask_results = set()
        while not self.event_handler.subtask_results.empty():
            subtask_results.add(self.event_handler.subtask_results.get())
        return subtask_results

    def get_lwd_hits(self):
        """Get a set of tuples of lwd hits (recipe_id, task_id)."""
        src_queue = self.event_handler.lwd_hits
        lwd_hits = set()
        while not src_queue.empty():
            lwd_hits.add(src_queue.get())
        return lwd_hits


class WatchedFile:
    """File that is being watched."""

    def __init__(self, path):
        """Create the object."""
        try:
            self.fhandle = open(path, 'rb')
        except FileNotFoundError:
            LOGGER.info('* File "%s" was removed!', path)
            self.fhandle = None

        self.position = 0
        self.path = path

    @classmethod
    def find_by_path(cls, path, lst):
        """Find WatchedFile in a lst by path, return None if not found."""
        for item in lst:
            if item.path == path:
                return item

        return None


class Handler(FileSystemEventHandler):
    """Process file changes."""

    # Regex for subtask results
    rgx_subtask = re.compile(rb'\*\* (.*?) (.*?) Score:([0-9]+)')
    # Regex for lwd hit
    rgx_lwd = re.compile(rb'Localwatchdog task: ([0-9]+)')

    def __init__(self, sem_lwd_sync):
        """Create the object."""
        # A list of watched files
        self.watched_files = []
        # A queue to store details about LWD hits
        self.lwd_hits = Queue()
        # A queue for subtask results
        self.subtask_results = Queue()
        # Ensures results aren't processed before event handler finishes
        self.sem_lwd_sync = sem_lwd_sync

    def _read_and_print(self, watched_file):
        """Read new subtask results in watched_file."""
        if watched_file.position:
            # When beginning a new read, return to where we stopped last time.
            watched_file.fhandle.seek(watched_file.position)

        # read data from file
        rawdata = watched_file.fhandle.read()

        # save position where we stopped
        watched_file.position += len(rawdata)
        recipe_id = int(watched_file.path.split('recipes/')[1].split('/')[0])
        # match lines against regex to detect subtask results
        lwd_matches = self.rgx_lwd.findall(rawdata)
        for match in lwd_matches:
            try:
                task_id = match.decode('utf-8')
                print(f'#{recipe_id}: task_id {task_id} hit LWD.')
            except UnicodeDecodeError:
                LOGGER.error('** Failed to process LWD hit!')
                continue

            self.lwd_hits.put((recipe_id, int(task_id)))

        subtask_matches = self.rgx_subtask.findall(rawdata)
        for testname, result, score in subtask_matches:
            task_id = int(watched_file.path.split('tasks/')[1].split('/')[0])
            try:
                testname, result, score = testname.decode('utf-8'), \
                                          result.decode('utf-8'), \
                                          score.decode('utf-8')
            except UnicodeDecodeError:
                LOGGER.error('** Failed to process subtask result!')
                continue
            else:
                score = int(score)
                # Put results in the queue, they will be merged with results
                self.subtask_results.put((recipe_id, task_id, testname, result,
                                          score))
                if '/10_localwatchdog' in testname:
                    self.lwd_hits.put((recipe_id, int(task_id)))

                # Log this
                print(f'Recipe: #{recipe_id}: Subtask {testname}: {result} Score: {score}')

    def on_any_event(self, event):
        """Print and store subtask results of taskout.log and harness.log files."""
        if not event.is_directory and ('taskout.log' in event.src_path or 'harness.log' in event.src_path) and \
                event.event_type in ('created', 'modified'):
            with self.sem_lwd_sync:
                # Get the object, if we're already watching the file.
                watched_file = WatchedFile.find_by_path(event.src_path,
                                                        self.watched_files)
                if watched_file is None:
                    # Create watched file object
                    watched_file = WatchedFile(event.src_path)
                    # Save it to a list of files we're watching
                    self.watched_files.append(watched_file)

                if watched_file.fhandle:
                    # read the file and print results
                    self._read_and_print(watched_file)
