#!/usr/bin/env python3
"""Running tests in standalone restraint."""
import itertools
import os

from twisted.internet import reactor, task, error

from cki_lib import misc
from provisioners.format import ProvisionData
from provisioners.interface import ProvisionerCore
from restraint_wrap.restraint_protocol import RestraintClientProcessProtocol
from upt.logger import LOGGER
from upt.misc import RET


class RunnerGlue:
    # pylint: disable=too-many-instance-attributes,no-member
    """Glue logic to process restraint client output."""

    def __init__(self, **kwargs):
        """Create the object."""
        # cmd-line arguments
        self.kwargs = kwargs
        # path to output directory
        self.output = kwargs['output']
        # number of reruns a task can have
        self.reruns = kwargs['reruns']

        self.provisioner_data = ProvisionData.deserialize_file(kwargs['input'])
        self.provisioners = self.provisioner_data.provisioners()

        # All twisted restraint client protocols
        self.protocols = []
        for provisioner in self.provisioners:
            # Hack: override keycheck value. Constructor of provisioner objects doesn't allow us to pass params,
            # because it would complicate (de)serializing the object. Instead of doing that or completely revising
            # a working API, override this one value.
            provisioner.keycheck = kwargs.get('keycheck')

            for i, resource_group in enumerate(provisioner.rgs):
                if resource_group.preprovisioned:
                    print(f'* [{str(provisioner)}] Found pre-provisioned resource group #{i}')
                    continue

                self.add_protocol(provisioner, resource_group, **kwargs)

        # was cleanup handler completed?
        self.cleanup_done = False

        # Use some all-encompassing error code
        self.retcode = RET.TOO_MANY_ERRORS

    def start_protocols(self):
        """Start restraint protocols one by one."""
        for proto in self.protocols:
            if not proto.proc:
                LOGGER.debug('Starting protocol for resource_id %s...', proto.resource_group.resource_id)
                # Create & start protocol's processes, register cleanup handlers
                # and let user know.
                proto.start_all()

                # Start one by one
                break
            if proto.proc and not proto.run_path_suffix:
                # Don't start until directory is created
                break

    def wait_on_protocols(self):
        """Wait for hosts to finish; protocols set flags in hosts."""
        self.start_protocols()

        all_rgs = itertools.chain(*[provisioner.rgs for provisioner in self.provisioners])
        if ProvisionerCore.all_recipes_finished(all_rgs) and self.protocols:
            # If processing of all hosts is done, then stop reactor, allowing
            # the script to exit.
            if reactor.running:
                try:
                    reactor.callFromThread(reactor.stop)
                except error.ReactorNotRunning:
                    pass

    def add_protocol(self, provisioner, resource_group, **kwargs):
        """Add a restraint client wrapper (twisted protocol)."""
        # Ensure hosts stay up for expected time
        for host in provisioner.find_objects([resource_group], lambda obj: obj if hasattr(obj,
                                                                                          'recipe_id') else None):
            provisioner.set_reservation_duration(host)

        # restraint client protocol and process
        proto = RestraintClientProcessProtocol(provisioner, resource_group, self.add_protocol, **kwargs)

        # Add to the list of protocols.
        self.protocols.append(proto)

        return proto

    def run(self):
        # pylint: disable=no-member
        """Run restraint client shell cmd."""
        reactor.addSystemEventTrigger('before', 'shutdown',
                                      self.cleanup_handler)

        print('Runner waiting for processes to finish...')

        protocols_wait_loop = task.LoopingCall(self.wait_on_protocols)
        protocols_defer = protocols_wait_loop.start(5.0)
        protocols_defer.addErrback(self.handle_runner_failure)
        protocols_defer.addCallback(self.handle_runner_failure)

        reactor.run()

        return self.retcode

    @classmethod
    def handle_runner_failure(cls, failure):
        """Print failure that occured in runner."""
        LOGGER.error('FAILURE in deferred: %s', failure)

    def cleanup_handler(self):
        # pylint: disable=protected-access
        """Do cleanup and evaluate retcodes from hosts."""
        if self.cleanup_done:
            return

        LOGGER.debug('* Runner cleanup handler runs...')

        # Aggregate retcodes from hosts. Prioritize kernel failure and
        # infrastructure issues. Other issues are printed to stderr.
        extra_retcodes = []
        for proto in self.protocols:
            extra_retcodes += proto.extra_retcodes
            if proto.proc:
                term_reason = proto.proc._getReason(proto.proc.status)
                if 'ended by signal 2' in str(term_reason):
                    extra_retcodes.append(RET.RUN_USER_ABORTED)

        all_rgs = itertools.chain(*[provisioner.rgs for provisioner in self.provisioners])
        self.retcode = self.eval_retcodes(all_rgs, extra_retcodes)

        if self.retcode == RET.PASSED_TESTING:
            print('* Kernel PASSED testing!')
        else:
            LOGGER.error('\n*** %s ***', self.retcode)

        # Detect abnormal exit in master instance and ensure termination.
        # It is highly suspicious when cleanup_handler runs and the hosts
        # aren't marked as "done_processing" yet.
        abnormal_exit = False
        for provisioner in self.provisioners:
            for host in provisioner.get_all_hosts(provisioner.rgs):
                # Force processing to end
                if not host._done_processing:
                    host._done_processing = True
                    abnormal_exit = True

        if not self.kwargs['dev_mode']:
            # Don't release resources during development, we would likely need to provision them again.
            for provisioner in self.provisioners:
                provisioner.release_resources()

        if abnormal_exit:
            print('* Abnormal exit!')

        self.cleanup_done = True

    def do_main(self):
        """Do all."""
        os.makedirs(self.output, exist_ok=True)

        # change directory, return on context end
        with misc.enter_dir(self.output):
            return self.run()

    @classmethod
    def eval_retcodes(cls, resource_groups, extras):
        # pylint: disable=no-else-return,protected-access,too-many-return-statements
        """Check hosts and return the highest priority retcode."""
        retcodes = extras.copy()
        retcodes_by_recipe_id = []
        for resource_group in resource_groups:
            # Don't consider retcodes from pre-provisioned resource groups
            if resource_group.preprovisioned:
                continue
            for host in resource_group.recipeset.hosts:
                host_retcode = RET.INFRASTRUCTURE_ISSUE if host._retcode is None else host._retcode
                retcodes_by_recipe_id.append(f'{host.recipe_id}:{host_retcode}')
                retcodes.append(host_retcode)

        print('All retcodes: %s', retcodes_by_recipe_id)

        if RET.RESTRAINT_PROBLEM in retcodes:
            # Restraint issue
            return RET.RESTRAINT_PROBLEM

        if RET.RUN_USER_ABORTED in retcodes:
            # Ctrl-c pressed
            return RET.RUN_USER_ABORTED

        if len(set(retcodes)) == 1 and retcodes[0] == RET.PASSED_TESTING:
            # All passed? Good!
            return RET.PASSED_TESTING
        elif RET.FAILED_TESTING in retcodes:
            # A failed kernel testing takes priority, we have to report that.
            return RET.FAILED_TESTING
        elif RET.INFRASTRUCTURE_ISSUE in retcodes:
            # Infrastructure issues are also imporant
            return RET.INFRASTRUCTURE_ISSUE

        # Otherwise return first error retcode
        for retcode in retcodes:
            if retcode != RET.PASSED_TESTING:
                return retcode

        # Shouldn't get here
        LOGGER.info('All retcodes: %s', retcodes)
        return RET.TOO_MANY_ERRORS
