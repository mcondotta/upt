"""Restraint client protocol; wrapper around restraint client binary."""
import os
import shlex

from bs4 import BeautifulSoup as BS
from twisted.internet import defer, protocol, reactor, error
from twisted.internet import task as twisted_task

from provisioners.interface import ProvisionerCore
from provisioners.beaker import Beaker
from restraint_wrap import const as restraint_const
from restraint_wrap.actions import ActionOnResult
from restraint_wrap.console import ConsoleProtocol
from restraint_wrap.restraint_host import RestraintHost
from restraint_wrap.restraint_shell import ShellWrap
from restraint_wrap.task_result import TaskResult
from restraint_wrap.watcher import Watcher
from upt import const
from upt.logger import LOGGER
from upt.misc import RET


# Disable invalid warning related to twisted.
# pylint: disable=no-member,protected-access

class RestraintClientProcessProtocol(protocol.ProcessProtocol):
    # pylint: disable=too-many-instance-attributes,too-many-locals,too-many-arguments,too-many-public-methods
    """Restraint binary processor."""

    def __init__(self, provisioner, resource_group, add_protocol, **kwargs):
        """Create the object."""
        self.restraint_binary = 'restraint'
        self.provisioner = provisioner  # type: ProvisionerCore
        self.resource_group = resource_group

        self.add_protocol = add_protocol

        self.kwargs = kwargs

        # Subdirectory created by restraint client, e.g './job.01'
        self.run_path_suffix = None

        # We don't care about the initial retcode value much, but set it to
        # some kind of failure.
        self.retcode = RET.INFRASTRUCTURE_ISSUE
        # Put auxillary info (e.g. restraint issues) here.
        self.extra_retcodes = []
        self.cleanup_done = False

        # Generate shell commands and possibly ssh aliases for restraint
        # client. These are to be kept in a temporary directory, to allow
        # inspection and debugging.
        self.wrap = ShellWrap(resource_group, **self.kwargs)

        # Here are hosts that we're processing input from.
        # Load expected hosts to improve parsing of restraint output
        self.rst_hosts = RestraintHost.from_line(self.wrap.restraint_commands)

        # A list of console objects that spy consoles of machines
        self.consoles = []

        # craft output file prefix
        self.xml_name = const.RSTRNT_JOB_XML.replace('.xml', '')

        self.dev_mode = kwargs['dev_mode']
        # How many times can we re-run a task
        self.reruns = kwargs['reruns']

        # This watches subtask results.
        self.watcher = Watcher()

        self.sem_lwd_sync = self.watcher.sem_lwd_sync

        self.recipe_ids_dead = set()
        self.ewd_recipe_ids_tasks = set()

        # A set of tuples (recipe_id, task_id) of lwd hits that weren't
        # processed
        self.unprocessed_lwd_hits = set()
        # A set of tuples (recipe_id, task_id, testname, result, score) of
        # subtask results that were not processed
        self.unprocessed_subtask_results = set()
        self.result_actions = self._prepare_actions()

        # twisted restraint client process
        self.proc = None
        self.heartbeat_loop = None

    def host_hearbeat(self):
        """Possibly set retcodes to all hosts that failed heartbeat."""
        # Get a list of hostnames that provisioner thinks are "dead".
        self.provisioner.heartbeat(self.resource_group, self.recipe_ids_dead,
                                   self.ewd_recipe_ids_tasks)

        for host in self.resource_group.recipeset.hosts:
            if host.recipe_id in self.recipe_ids_dead and not \
                    host._done_processing:
                LOGGER.debug('** Marking %i as dead -> restraint '
                             'client process will end', host.recipe_id)
                # We only care about setting this to hosts that aren't
                # finished processing.
                # If the host is done processing, retcode won't change
                for rcp_id, task_result in self.ewd_recipe_ids_tasks:
                    if host.recipe_id != rcp_id:
                        continue
                    self.process_task(host, task_result.task_id, task_result.status, task_result.result,
                                      task_result.statusresult)

                # We won't process any more tasks on this one
                host._done_processing = True
                try:
                    self.proc.signalProcess('TERM')
                except error.ProcessExitedAlready:
                    pass

    def outReceived(self, data):
        """Process incoming stdout data from restraint client."""
        data = data.decode('utf-8')
        # keep all stdout output
        if data.strip():
            # Log all stdout data
            LOGGER.debug(data.strip())

        if not self.heartbeat_loop:
            self.heartbeat_loop = twisted_task.LoopingCall(self.host_hearbeat)
            self.heartbeat_loop.start(5.0)

        # run heartbeat check on connectivity issues
        defer_heartbeat = defer.Deferred()
        defer_heartbeat.addErrback(self.handle_failure)
        defer_heartbeat.callback(data)

        if data.strip():
            self.find_host_lines(data.strip())

    def handle_failure(self, failure):
        """Log exception, set error retcode and terminate restraint client."""
        # pylint: disable=no-member
        LOGGER.error("Fatal exception caught %s", failure.getTraceback())
        # Don't stop reactor, that would stop all the instances. Mark this as
        # a restraint problem and terminate the restraint client process, if
        # it isn't already.
        self.extra_retcodes.append(RET.RESTRAINT_PROBLEM)
        try:
            self.proc.signalProcess('TERM')
        except error.ProcessExitedAlready:
            pass

    def _check_stderr(self, data):
        """Check restraint client stderr messages for error indications."""
        result = restraint_const.RGX_LAB_WATCHDOG.findall(data)
        for hit_recipe_id in result:
            print(f'*** Recipe {hit_recipe_id} hit lab watchdog!')
            self.recipe_ids_dead.add(int(hit_recipe_id))
            return

        for err in restraint_const.ERROR_INDICES:
            if err.findall(data):
                LOGGER.error("Exiting because: %s", data)
                self.extra_retcodes.append(RET.RESTRAINT_PROBLEM)

                if reactor.running:
                    try:
                        reactor.callFromThread(reactor.stop)
                    except error.ReactorNotRunning:
                        pass

    def errReceived(self, data):
        """Process incoming stderr data from restraint client."""
        data = data.decode('utf-8')
        if data.strip():
            # Log all stderr data
            LOGGER.debug(data.strip())

        defer_stderr_handling = defer.Deferred()
        defer_stderr_handling.addCallback(self._check_stderr)
        defer_stderr_handling.addErrback(self.handle_failure)
        # check stderr output for fatal errors
        defer_stderr_handling.callback(data)

    def identify_host(self, line):
        """Identify host output in restraint client."""
        match_result = RestraintHost.rgx_host.fullmatch(line)
        if match_result:
            self.rst_hosts.append(RestraintHost(*match_result.groups()))
            return True
        return False

    def start_consoles(self):
        """Start all console protocols for Beaker."""
        # Console module is currently used for Beaker only. See issues #8 and #9 in Gitlab.
        if self.kwargs['console'] and isinstance(self.provisioner, Beaker):
            for host in ProvisionerCore.get_all_hosts([self.resource_group]):
                self.consoles.append(ConsoleProtocol(host, **self.kwargs))

    def filter_output(self, line):
        """Get an output line that belongs to a certain hostname."""
        # look for starting lines about hostname/recipe_ids
        if self.identify_host(line):
            # "Connecting to" line; nothing else do be done
            return None

        new_run = restraint_const.RGX_NEW_RUN.match(line)
        if new_run:
            self.kwargs['run_path_suffix'] = self.run_path_suffix = new_run.group(1)
            self.start_consoles()

            path2watch = os.path.join(f'{os.path.abspath(self.kwargs["output"])}',
                                      os.path.relpath(self.run_path_suffix))
            # Start watcher with exact path
            reactor.callInThread(self.watcher.run, path2watch)

        # try to figure out to which host the output line belongs
        match_result = RestraintHost.rgx_host_line.findall(line)
        if not match_result:
            # one more line to filter out: info about where output is saved
            for msg in restraint_const.KNOWN_MESSAGES:
                if msg in line:
                    LOGGER.info(line.strip())
                    return None

            # unknown output
            LOGGER.info('!!! Received unknown output "%s"', line)
            return None

        return match_result

    def split_hostline_match(self, host, hostline):
        """Parse a line from a hostname."""
        idx = RestraintHost.in_list(host, self.rst_hosts)
        if idx is not None:
            hostobj = self.rst_hosts[idx]  # type: RestraintHost
            hostobj.lines.append(hostline)
            try:
                regex2use = RestraintHost.rgx_state_change
                task_id, testname, status, result = regex2use.findall(
                    hostline)[0]
                # Sanitize output !
                task_id = int(task_id)
            except (AttributeError, TypeError, IndexError):
                # This could be score line details like:
                # 81955317 [sanity                          ] PASS Score: 0
                pass
            else:
                return (hostobj, task_id, testname.strip(), status, result)
        else:
            LOGGER.info('!!! Received line from unknown host %s', host)
            LOGGER.info('* line: "%s"', hostline)

        return None

    def find_host_lines(self, line):
        """Process restraint output."""
        match_result = self.filter_output(line)
        if not match_result:
            return

        for hostobj, hostline in match_result:
            tup = self.split_hostline_match(hostobj, hostline)
            if not tup:
                continue
            hostobj, task_id, testname, status, result = tup
            statusresult = status if not result else f'{status}: {result}'

            LOGGER.debug('*** New restraint client output ***')
            print(f'Recipe: #{hostobj.recipe_id}: Host {hostobj.realname}: {statusresult}: {testname.strip()}')

            host = self.provisioner.find_host_object(hostobj.recipe_id)
            # Pass params to ActionOnResult objects, so we keep evaluating
            # result and do further actions, such as re-running some tasks.
            self.process_task(host, task_id, status, result, statusresult)

    def cleanup_handler(self):
        """Save console data, tell related threads to stop."""
        if self.cleanup_done:
            return

        # Client process ended, nothing to do. Reruns will have to be done by another restraint protocol object.
        LOGGER.debug('restraint protocol cleanup runs (rid: %s)...', self.resource_group.resource_id)

        recipeset = self.resource_group.recipeset
        recipeset._tests_finished = True
        do_rerun = recipeset._attempts_made <= self.reruns if recipeset._waiting_for_rerun else False
        # Careful! Clear the flag AFTER evaluating re-run condition.
        recipeset._waiting_for_rerun = False

        if do_rerun:
            # Add a new protocol
            proto = self.add_protocol(self.provisioner, self.resource_group, **self.kwargs.copy())
            proto.start_all()
        else:
            # If we're not rerunning anything, then recipeset is finished.
            for host in recipeset.hosts:
                host._done_processing = True

        # Stop watcher.
        self.watcher.observer.stop()

        # Save console output
        for console in self.consoles:
            console.save_and_terminate()

        self.cleanup_done = True

    def processEnded(self, reason):
        # pylint: disable=no-member
        """Cleanup on process exit, handle retcode if it was caused by user."""
        # Log that restraint client process ended
        result = [f'R:{r.recipe_id}' for r in self.provisioner.find_objects([self.resource_group], lambda obj: obj if
                                                                            hasattr(obj, 'recipe_id') else None)]
        LOGGER.info('* restraint protocol (%s) %s retcode: %s reason: %s', result, 'was SIGPIPEd!' if
                    'ended by signal 13' in str(reason.value) else 'ended.', reason.value.exitCode, reason.value)

        self.cleanup_handler()

    def rerun_from_task(self, **kwargs):
        """Ensure task(s) are re-run for a host."""
        host = kwargs['host']
        recipeset = self.resource_group.recipeset

        # Set _waiting_for_rerun and let tests finish, if they can.
        if not recipeset._waiting_for_rerun:
            recipeset._waiting_for_rerun = True
            # Restraint numbers task_id from 1.
            host._rerun_recipe_tasks_from_index = kwargs['task_id'] if host._rerun_recipe_tasks_from_index is None \
                else host._rerun_recipe_tasks_from_index + kwargs['task_id'] - 1

            # Increase the counter of attempts. Entire recipeSet has to be re-run.
            recipeset._attempts_made += 1

            task_result = kwargs['task_result']

            LOGGER.info('Recipe: #%i: Task %s (T:%s): Making a re-run attempt %i/%i for the entire recipeSet',
                        task_result.recipe_id, task_result.testname, task_result.task_id, recipeset._attempts_made,
                        self.kwargs['reruns'])

    def add_task_result(self, **kwargs):
        """Save task results to a RestraintHost object."""
        host = self.provisioner.find_host_object(int(kwargs['recipe_id']))

        task_result = TaskResult(kwargs['recipe_id'], kwargs['task'],
                                 kwargs['task_id'],
                                 kwargs['result'], kwargs['status'],
                                 kwargs['prev_task'], host=host)

        # Load all results, regardless of whether it's for this result
        self.unprocessed_subtask_results = self.unprocessed_subtask_results.union(self.watcher.get_subtask_results())

        to_remove = set()
        # Link subtask results
        for recipe_id, task_id, testname, result, score in self.unprocessed_subtask_results:
            if recipe_id == task_result.recipe_id and task_id == task_result.task_id:
                task_result.subtask_results.append((testname, result, score))
                to_remove.add((recipe_id, task_id, testname, result, score))

        # Remove processed tuples from unprocessed_subtask_results
        self.unprocessed_subtask_results = self.unprocessed_subtask_results - to_remove

        host._task_results.append(task_result)

        return task_result

    def mark_recipe_done(self, **kwargs):
        """Find host by recipe_id and mark it as done processing.

        Whenever this method is called, it will mark host as 'done_processing' and it will be impossible to change the
        retcode (overall kernel testing result) for that host.
        """
        task_result = kwargs['task_result']
        recipe_id = task_result.orig_recipe_id if task_result.orig_recipe_id \
            else task_result.recipe_id

        if task_result.recipe_id != kwargs['recipe_id']:
            raise RuntimeError('broken code: invalid data provided')

        host = self.provisioner.find_host_object(recipe_id)
        if not host._done_processing:
            host._done_processing = True
            print(f"Recipe: #{host.recipe_id} is done processing [mark_recipe_done]")

    def mark_lwd_hit(self, **kwargs):
        """Mark LWD hit as processed."""
        task_result = kwargs['task_result']
        # New LWD hit detected (and consumed by this task)
        self.unprocessed_lwd_hits.remove((task_result.recipe_id,
                                          task_result.task_id))

    def _prepare_actions(self):
        # Determines what actions to make on a specific result combination.
        # See example use comment in the object itself.
        return [
            ActionOnResult([self.mark_recipe_done],
                           '(EWD hit + Panic means kernel failed testing.)',
                           RET.FAILED_TESTING,
                           # waived=False,
                           ewd_hit={True}, result={'Panic'}),

            ActionOnResult([self.mark_recipe_done],
                           '(EWD hit, but no Panic is an infrastructure issue.'
                           ' No reruns will be done.)',
                           RET.INFRASTRUCTURE_ISSUE,
                           # waived=False,
                           ewd_hit={True}),

            ActionOnResult([self.mark_lwd_hit],
                           '(LWD hit. This type of issue is ignored'
                           ' completely.)',
                           None,
                           waived={False}, lwd_hit={True}),

            # Previous task of a recipe was waived and panicked, which causes
            # the next task to abort. The task is waived for a reason, don't
            # fail testing, but mark the recipe as processed. TESTING PASSED.
            ActionOnResult([self.mark_recipe_done],
                           '(Testing was all good, until a waived task '
                           'panicked. Recipe processing finished.)',
                           RET.PASSED_TESTING,
                           result={'Warn'}, waived={False}, status={'Aborted'},
                           prv_tsk_panicked_waived={True}),

            # A non-waived task panicked -> kernel failed testing on this host.
            # Don't confuse this with infrastructure errors and mark the recipe
            # as done, so we don't change the retcode.
            ActionOnResult([self.mark_recipe_done],
                           '(A non-waived task panicked. Kernel testing '
                           'failed. Recipe processing finished.)',
                           RET.FAILED_TESTING,
                           result={'Panic'}, waived={False}),

            ActionOnResult([self.mark_recipe_done],
                           '(A non-waived task aborted + ran out of retries.)',
                           RET.INFRASTRUCTURE_ISSUE,
                           waived={False}, status={'Aborted'},
                           out_of_reruns={True}, lwd_hit={False}, waiting_for_rerun={False}),

            # A non-waived tasked aborted. This is a possible infrastructure
            # issue, DON'T mark recipe as done and respin the task(s).
            ActionOnResult([self.rerun_from_task],
                           '(A non-waived task aborted. Re-running task to'
                           ' confirm infrastructure issue.)',
                           None,
                           waived={False}, status={'Aborted'},
                           out_of_reruns={False}, lwd_hit={False}, waiting_for_rerun={False}),

            # The rest of the fall-through conditions.
            ActionOnResult([self.mark_recipe_done],
                           '(A non-waived task has warnings. Kernel testing '
                           'failed. Recipe processing finished.)',
                           RET.FAILED_TESTING,
                           result={'Warn'}, waived={False}),

            ActionOnResult([self.mark_recipe_done],
                           '(A non-waived task failed. Kernel testing '
                           'failed. Recipe processing finished.)',
                           RET.FAILED_TESTING,
                           result={'FAIL'}, waived={False}),

            # NOTE: this is the 1st of 2 places where the RET.PASSED_TESTING retcode is set. If any previous tasks
            # failed or aborted, the testing result should be decided by then and this is never allowed to execute.
            # This only executes if we're not waiting for re-run results.
            ActionOnResult([self.mark_recipe_done], '(Last test passed OK. Kernel testing passed.)',
                           RET.PASSED_TESTING, result={'PASS'}, status={'Completed'}, is_last_host_task={True},
                           waiting_for_rerun={False}),

            # NOTE: this is the 2nd of 2 places where the RET.PASSED_TESTING retcode is set. If last test is waived,
            # we make the kernel testing pass, because if any previous tasks aborted or failed, the retcode for this
            # host would already be set.
            # This only executes if we're not waiting for re-run results.
            ActionOnResult([self.mark_recipe_done], '(Last test is waived, previous tests did not fail. '
                                                    'Kernel testing passed.)',
                           RET.PASSED_TESTING, waived={True}, status={'Completed', 'Aborted'},
                           is_last_host_task={True}, waiting_for_rerun={False})
        ]

    def is_ewd_hit(self, recipe_id, status):
        """Check if a given recipe_id had an EWD hit."""
        LOGGER.debug('is_ewd_hit: %i %s %s', recipe_id, status,
                     self.recipe_ids_dead)
        if recipe_id in self.recipe_ids_dead and status == 'Aborted':
            return True

        return False

    def is_lwd_hit(self, recipe_id, task_id):
        """Check if a given recipe_id:task_id had a LWD hit."""
        # Load new LWD hits and add them to the unprocessed ones.
        self.unprocessed_lwd_hits = self.unprocessed_lwd_hits.union(
            self.watcher.get_lwd_hits())

        LOGGER.debug('LWD hit check: is %i %s in %s?', recipe_id, task_id,
                     self.unprocessed_lwd_hits)

        if (int(recipe_id), int(task_id)) in self.unprocessed_lwd_hits:
            host = self.provisioner.find_host_object(int(recipe_id))
            for i, task_result in enumerate(host._task_results):
                if task_result.task_id == task_id:
                    # Save the information that task hit LWD into task itself.
                    # This is important so we can make sure that recipe isn't
                    # marked as done, task isn't rerun, yet testing finishes.
                    host._task_results[i].lwd_hit = True

            return True

        return False

    def is_last_host_task(self, recipe_id, task_id):
        """Return True if this is the last task that the host should run according to its restraint_xml.

        Arguments:
            recipe_id - int, the id that uniquely identifies the host
        """
        restraint_xml = None
        for temp_host in self.resource_group.recipeset.hosts:
            if temp_host.recipe_id == recipe_id:
                restraint_xml = self.resource_group.recipeset.restraint_xml
                break

        assert restraint_xml is not None, "Invalid data or broken code. Cannot find restraint_xml of host."

        recipe = BS(restraint_xml, 'xml').find('recipe', {'id': recipe_id})
        task_count = len(recipe.findAll('task'))
        return task_count == int(task_id)

    def evaluate_task_result(self, task_result, host):
        """Evaluate the result of task_result in relation to a host."""
        ac_kwargs = {'recipe_id': task_result.recipe_id,
                     'task_id': task_result.task_id,
                     'testname': task_result.testname,
                     'task': task_result.task,
                     'status': task_result.status,
                     'result': task_result.result,
                     'waived': task_result.waived,
                     'lwd_hit': self.is_lwd_hit(task_result.recipe_id,
                                                task_result.task_id),
                     'ewd_hit': self.is_ewd_hit(task_result.recipe_id,
                                                task_result.status),
                     'host': host,
                     'task_result': task_result,
                     'prv_tsk_panicked_waived':
                         task_result.prv_tsk_panicked_waived,
                     'out_of_reruns': (self.resource_group.recipeset._attempts_made >= self.reruns),
                     'is_last_host_task': self.is_last_host_task(task_result.recipe_id, task_result.task_id),
                     'waiting_for_rerun': self.resource_group.recipeset._waiting_for_rerun}

        with self.sem_lwd_sync:
            # Go through predefined actions based on results
            if not host._done_processing:
                for action in self.result_actions:
                    # Check if conditions are fulfilled.
                    if action.eval(**ac_kwargs):
                        # Debug: store data about what the conditions were
                        LOGGER.debug(str(action))

                        # Set this host's "retcode"!
                        if action.retcode is not None:
                            host._retcode = action.retcode

                        fmt_args = {'host': host,
                                    'recipe_id': host.recipe_id,
                                    'statusresult': task_result.statusresult,
                                    'testname': task_result.testname}

                        msg = action.msg_string.format(**fmt_args)
                        print(msg)

                        # If so, run all methods that must run in this case.
                        for func in action.lst_actions:
                            func(**ac_kwargs)

                        # DO NOT PROCESS OTHER ACTIONS IN THE LIST
                        break

    def get_restraint_xml_task(self, recipe_id, task_id):
        """Get task of a recipe by recipe_id and task_id."""
        soup = self.wrap.run_soup
        recipe = soup.find('recipe', id=recipe_id)
        tasks = recipe.find_all('task')

        try:
            return tasks[task_id - 1]
        except (IndexError, AttributeError):
            LOGGER.error('task T:%i not found in recipe %s!', task_id, recipe_id)

    def process_task(self, host, task_id, status, result, statusresult):
        # pylint: disable=too-many-arguments,too-many-locals
        """Evaluate the result of the task and how it affects how we go on."""
        # Get the task from provisioner data
        task = self.get_restraint_xml_task(host.recipe_id, task_id)
        if not task:
            LOGGER.error('%i: %d to rerun was not found!', host.recipe_id,
                         task_id)
            return None

        # Get flags: is task waived, was previous task waived and did it panic?
        try:
            prev_task = host._task_results[-1]
        except IndexError:
            prev_task = None

        # Save task result in natural order per recipe_id
        task_result = self.add_task_result(recipe_id=host.recipe_id,
                                           task_id=task_id,
                                           task=task,
                                           result=result,
                                           status=status,
                                           prev_task=prev_task)

        return self.evaluate_task_result(task_result, host)

    def start_all(self):
        """Create & start process, register cleanup handlers."""
        cmds = self.wrap.restraint_commands
        print(f'* Running "{cmds}"...')
        self.proc = reactor.spawnProcess(self, self.restraint_binary, shlex.split(cmds), env=os.environ)

        reactor.addSystemEventTrigger('before', 'shutdown', self.cleanup_handler)
