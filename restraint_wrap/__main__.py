#!/usr/bin/env python3
"""Main entry point, command-line interface for upt."""
import os
import logging
import sys

import click

from cki_lib.logger import logger_add_fhandler
from rcdefinition.rc_data import SKTData
from upt.logger import LOGGER

from restraint_wrap.runner import RunnerGlue
from restraint_wrap.misc import add_directory_suffix

CONTEXT_SETTINGS = dict(help_option_names=['-h', '--help'])


@click.group(context_settings=CONTEXT_SETTINGS)
def cli():
    """Run test using restraint's standalone mode."""


@cli.command()
@click.option('-i', '--input', required=True, default='c_req.yaml',
              help='Path to input file (provisioning request). '
                   'Default: c_req.yaml.')
@click.option('-o', '--output', required=True, default='run',
              help='Path to output directory. Default: run.done.01.'
                   'Numerical suffix .XY will be added if the directory '
                   'exists. This prevent issues with watching subtask results '
                   'in files.')
@click.option('--keycheck/--no-keycheck', default=False,
              help='Do strict ssh host keychecking. Default: False.')
@click.option('--dev-mode/--no-dev-mode', required=True, default=True,
              help='If False, print extra info on fail.')
@click.option('--conf-cons', required=False, default='conserver.yaml',
              help='Path to a yaml file. If set, these alternative conserver'
                   ' servers will be used based on server location. See'
                   ' conserver.yaml.template')
@click.option('--console/--no-console', required=False, default=True,
              help='If True, save host console output. Default: True.')
@click.option('--reruns', required=True, default=3, type=int,
              help='How many times to re-run aborted tasks (including '
                   'EWD/LWD hits).')
@click.option('--rc', default=os.environ.get('RC_FILE', 'rc'), help='Path to rc file. Defaults to RC_FILE env. '
                                                                    'variable or rc.')
def test(**kwargs):
    """Run tests using restraint."""
    output = kwargs['output'] = add_directory_suffix(kwargs['output'])
    logger_add_fhandler(LOGGER, 'info.log', output)

    # Open rc-file, deserialize it
    with open(kwargs['rc']) as fhandle:
        kwargs['rc_data'] = SKTData.deserialize(fhandle.read())

    if not kwargs['dev_mode']:
        for handler in LOGGER.handlers:
            handler.setLevel(logging.ERROR)

    print('restraint test runner started')

    if kwargs['conf_cons']:
        kwargs['conf_cons'] = os.path.abspath(kwargs['conf_cons'])

    kwargs['keycheck'] = 'yes' if kwargs['keycheck'] else 'no'
    glue = RunnerGlue(**kwargs)

    retcode = glue.do_main()

    sys.exit(retcode.value)


def main():
    """Do all."""
    cli()


if __name__ == '__main__':
    main()
