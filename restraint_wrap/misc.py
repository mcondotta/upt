"""Misc utility methods."""
import os


def add_directory_suffix(output):
    """Add numerical .done.XY suffix to directory."""
    dirname_template = os.path.abspath(output) + '.done'
    final_dirname = dirname_template
    i = 1
    while os.path.exists(final_dirname):
        final_dirname = dirname_template + f'.{i:02d}'
        i += 1

    return final_dirname
