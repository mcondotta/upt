"""Read system console."""

import os
import pathlib
import shlex

from twisted.internet import protocol, reactor, error

from cki_lib.yaml_wrap import RuamelSerializer

from upt import const
from upt.logger import LOGGER


# Disable invalid warning related to twisted.
# pylint: disable=no-member

# pylint: disable=too-many-instance-attributes

class ConsoleProtocol(protocol.ProcessProtocol):
    """Read system console."""

    binary = 'console'

    def __init__(self, host, **kwargs):
        """Create the object."""
        # Host object containing recipe_id and hostname
        self.host = host

        # Global output directory
        self.output = os.path.abspath(kwargs['output'])

        self.run_path_suffix = kwargs['run_path_suffix']

        # All captured stdout content
        self.stdout = b''
        # All captured stderr content
        self.stderr = b''

        # Craft path to stdout, stderr files
        basepath = pathlib.Path(self.output, f'{self.run_path_suffix}/recipes', str(self.host.recipe_id))
        basepath.mkdir(parents=True, exist_ok=True)
        self.stdout_path = os.path.join(basepath.absolute(), 'console.log')
        self.stderr_path = os.path.join(basepath.absolute(), 'console.log.stderr')

        self.server = const.DEF_CONSOLE_SERVER
        if kwargs['conf_cons'] and os.path.isfile(kwargs['conf_cons']):
            yaml = RuamelSerializer().deserialize_file(kwargs['conf_cons'])
            for txt, server in yaml.items():
                if txt in self.host.hostname:
                    self.server = server
                    break

        # Craft command to run console binary in spymode
        self.cmd = shlex.split(f'{self.binary} -M {self.server} '
                               f'{self.host.hostname} -s')

        self.proc = reactor.spawnProcess(self, self.binary, self.cmd, env=os.environ)
        self.finished = False

        LOGGER.debug('* reading %s console through %s', self.host.hostname,
                     self.server)

    def outReceived(self, data):
        """Process stdout stream."""
        # keep all stdout output
        if data:
            self.stdout += data

            if b'-- Console down' in data or b'-- Console up' in data:
                LOGGER.info(data)

    def errReceived(self, data):
        """Process stderr stream."""
        if data:
            self.stderr += data

    def write_files(self):
        """Write content of stderr & stdout files to disk."""
        if self.stderr:
            with open(self.stderr_path, 'wb') as fhandle:
                fhandle.write(self.stderr)

        with open(self.stdout_path, 'wb') as fhandle:
            fhandle.write(self.stdout)

    def save_and_terminate(self):
        """Save files and terminate process."""
        if not self.finished:
            self.finished = True
            self.write_files()
            try:
                self.proc.signalProcess('TERM')
            except error.ProcessExitedAlready:
                pass

    def processEnded(self, reason):
        """Handle everything that needs to happen on process exit, log info, write files and mark it as done."""
        # Log that console process finished running
        LOGGER.debug('* console process for %s exited', self.host.hostname)
        # Write console output to file and possibly stderr log for conserver
        self.write_files()
        self.finished = True
