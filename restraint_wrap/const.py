"""Restraint runner constants."""
import re

# regex for restraint client stderr; indicates lab watchdog timeout
RGX_LAB_WATCHDOG = re.compile(r'(?:Recipe ([0-9]+) exceeded lab '
                              r'watchdog timer)')
# a list of strings that indicate fatal restraint client error
ERROR_INDICES = [re.compile(x) for x in
                 ['Could not resolve hostname',
                  'Error writing to ssh channel',
                  'Unable to find matching host for recipe',
                  'connect to[^\n+] failure',
                  'Document failed validation.']]
# a list of strings that indicate message we just print
KNOWN_MESSAGES = ['Disconnected..', 'for job run']

# a message that is printed when a new restraint client run starts
RGX_NEW_RUN = re.compile(r'Using (.*?) for job run')
