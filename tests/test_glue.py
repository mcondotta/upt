"""Test cases for glue module."""
import os
import unittest
from unittest.mock import patch

from cki_lib.misc import tempfile_from_string

from provisioners.format import ProvisionData
from upt.glue import ProvisionerGlue
from upt.misc import RET

from tests.const import ASSETS_DIR


class TestGlue(unittest.TestCase):
    """Test cases for glue module."""

    def setUp(self) -> None:
        self.req_asset = os.path.join(ASSETS_DIR, 'req.yaml')
        self.prov_data = ProvisionData.deserialize_file(self.req_asset)

    def test_do_wait(self):
        """Ensure do_wait calls expected methods."""
        with patch.object(self.prov_data.beaker, 'wait', new=lambda *args, **kwargs: RET.PASSED_TESTING):
            with patch.object(self.prov_data.beaker, 'update_provisioning_request', new=lambda: None):
                with tempfile_from_string(b'') as fname:
                    glue = ProvisionerGlue(self.prov_data, **{'request_file': fname})
                    self.assertEqual(glue.do_wait(), RET.PASSED_TESTING)
                    glue = ProvisionerGlue(self.prov_data, **{'request_file': fname})
                    self.assertEqual(glue.do_wait(), RET.PASSED_TESTING)

    @patch('upt.glue.ProvisionerGlue.do_wait', return_value=RET.RUN_USER_ABORTED)
    @patch('provisioners.format.ProvisionData.serialize2file')
    @patch('provisioners.beaker.Beaker.provision')
    @patch('provisioners.beaker.Beaker.update_provisioning_request')
    def test_run_provisioners(self, _, __, ___, ____):
        """Ensure run_provisioners works and calls expected methods."""
        with tempfile_from_string(b'') as fname:
            glue = ProvisionerGlue(self.prov_data, **{'request_file': fname})
            self.assertEqual(RET.RUN_USER_ABORTED, glue.run_provisioners())
