"""Test cases for upt __main__ module."""
import unittest
from unittest import mock

from click.testing import CliRunner

from cki_lib.misc import tempfile_from_string
from upt.__main__ import cli, main

from tests.const import FakeProv


class TestUPT(unittest.TestCase):
    """Test cases for upt __main__ module."""

    def setUp(self):
        self.mock_logger_add_fhandler = mock.patch('upt.__main__.logger_add_fhandler', mock.Mock())
        self.mock_logger_add_fhandler.start()

    @mock.patch('provisioners.format.ProvisionData.provisioners', new=lambda x: [])
    @mock.patch('upt.logger.LOGGER.warning', mock.Mock())
    def test_provision_api_main(self):
        """Test that main click api (provision) works."""
        runner = CliRunner()

        with tempfile_from_string(b'') as rcfile:
            with tempfile_from_string(b'') as ignore:
                response = runner.invoke(cli, ['--rc', rcfile, 'provision', '-r', ignore])
                self.assertEqual(response.exit_code, 0)

    @mock.patch('builtins.print', mock.Mock())
    @mock.patch('provisioners.format.ProvisionData.provisioners', new=lambda x: [FakeProv()])
    def test_cancel_api(self):
        # pylint: disable=no-self-use
        """Test that click api (cancel) works."""
        runner = CliRunner()

        with tempfile_from_string(b'') as rcfile:
            with tempfile_from_string(b'') as ignore:
                response = runner.invoke(cli, ['--rc', rcfile, 'cancel', '-r', ignore])
                self.assertEqual(response.exit_code, 0)

    @mock.patch('upt.__main__.truncate_logs', mock.Mock())
    @mock.patch('upt.__main__.cli', mock.Mock())
    def test_main_method(self):
        # pylint: disable=no-self-use
        """Ensure main works."""

        main()

    @mock.patch('builtins.print', mock.Mock())
    @mock.patch('provisioners.aws_setup.AWSSetup.setup', new=lambda *args: None)
    def test_aws_setup_api(self):
        # pylint: disable=no-self-use
        """Test that click api (aws-setup) works."""
        runner = CliRunner()

        with tempfile_from_string(b'') as rcfile:
            runner.invoke(cli, ['--rc', rcfile, 'aws-setup', '-k', rcfile])

    @mock.patch('builtins.print', mock.Mock())
    @mock.patch('provisioners.aws_setup.AWSSetup.setup', new=lambda *args: None)
    def test_aws_setup_api_with_ip(self):
        """Test that click api (aws-setup) works."""
        runner = CliRunner()

        with tempfile_from_string(b'') as rcfile:
            response = runner.invoke(cli, ['--rc', rcfile, 'aws-setup', '-k', rcfile, '--ip', '0.0.0.0'])
            self.assertEqual(response.exit_code, 0)

    @mock.patch('builtins.print')
    @mock.patch('provisioners.format.ProvisionData.provisioners', new=lambda x: [])
    def test_waiton_api(self, mock_print):
        """Test that click api (waiton) works."""
        runner = CliRunner()

        with tempfile_from_string(b'') as rcfile:
            with tempfile_from_string(b'') as ignore:
                response = runner.invoke(cli, ['--rc', rcfile, 'waiton', '-r', ignore])
                self.assertEqual(response.exit_code, 0)

                mock_print.assert_any_call('UPT CLI started')
                mock_print.assert_any_call('Waiting on existing provisioning request...')
