"""Test constants."""
import os

ASSETS_DIR = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'assets')

AWS_UPT_CONFIG = """{
    "launch_template": "arr-cki.staging.lt.upt",
    "instance_prefix": "arr-cki.staging.i.upt"
}"""


class FakeProv:
    """Fake of a provisioner."""

    def __init__(self):
        """Create the object."""
        self.rgs = []

    def release_resources(self):
        """Fake of a provisioner method."""
