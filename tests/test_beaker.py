"""Test cases for Beaker provisioner module."""
import copy
import os
import pathlib
import unittest
from unittest import mock

from bs4 import BeautifulSoup as BS

from cki_lib import misc

from provisioners.beaker import Beaker
from provisioners.format import ProvisionData
from restraint_wrap.task_result import TaskResult
from upt.misc import RET
from upt.legacy import convert_xml

from tests.const import ASSETS_DIR


class TestBeaker(unittest.TestCase):
    # pylint: disable=too-many-public-methods,protected-access
    """Test cases for Beaker module."""

    def setUp(self) -> None:
        self.req_asset = os.path.join(ASSETS_DIR, 'req.yaml')

        self.xmlpath = pathlib.Path(ASSETS_DIR, 'beaker_xml')
        self.xml = self.xmlpath.read_text()

    def test_exclude_hosts(self):
        """Ensure exclude_hosts works."""
        prov_data = ProvisionData.deserialize_file(self.req_asset)
        with misc.tempfile_from_string(b'hostname1\nhostname2') as fname:
            prov_data.beaker.exclude_hosts(fname)

        self.assertIn('<hostname op="!=" value="hostname1"/>',
                      prov_data.beaker.rgs[0].recipeset.beaker_hosts[0].recipe_fill)
        self.assertIn('<hostname op="!=" value="hostname2"/>',
                      prov_data.beaker.rgs[0].recipeset.beaker_hosts[0].recipe_fill)

    def test_exclude_hosts_and(self):
        """Ensure exclude_hosts works with and element."""
        prov_data = ProvisionData.deserialize_file(self.req_asset)
        with misc.tempfile_from_string(b'hostname1\nhostname2') as fname:
            prov_data.beaker.exclude_hosts(fname)

        recipeset0 = prov_data.beaker.rgs[0].recipeset

        self.assertIn('<hostname op="!=" value="hostname1"/>', recipeset0.beaker_hosts[0].recipe_fill)
        self.assertIn('<hostname op="!=" value="hostname2"/>', recipeset0.beaker_hosts[0].recipe_fill)

        # Ensure the hostRequires element isn't changed when hostRequires force= is used.
        self.assertEqual('<hostRequires force="def"/>',
                         str(BS(recipeset0.beaker_hosts[1].recipe_fill, 'xml').find('hostRequires')))

        self.assertIn('<hostname op="!=" value="hostname1"/>', recipeset0.beaker_hosts[2].recipe_fill)
        self.assertIn('<hostname op="!=" value="hostname2"/>', recipeset0.beaker_hosts[2].recipe_fill)

        # Ensure hostname exclude isn't appended twice.
        expect = BS('<recipe ks_meta="re"><hostRequires><and><hostname op="!=" value="hostname2" />'
                    '<hostname op="!=" value="hostname1" /></and></hostRequires></recipe>',
                    'xml').find('recipe')
        self.assertEqual(expect.prettify(), prov_data.beaker.rgs[0].recipeset.beaker_hosts[2].recipe_fill)

    @mock.patch('builtins.print', mock.Mock())
    def test_update_provisioning_state(self):
        # pylint: disable=protected-access
        """Ensure update_provisioning_state works and flips a simple flag."""
        prov_data = ProvisionData.deserialize_file(self.req_asset)
        bkr = prov_data.beaker
        bkr.rgs.append(copy.deepcopy(bkr.rgs[0]))
        bkr.rgs[0]._provisioning_done = False
        bkr.rgs[1]._provisioning_done = True

        with mock.patch('provisioners.beaker.Beaker.is_provisioned', lambda *args: False):
            bkr.update_provisioning_state()
            self.assertFalse(bkr.rgs[0]._provisioning_done)

        with mock.patch('provisioners.beaker.Beaker.is_provisioned', lambda *args: True):
            bkr.update_provisioning_state()
            self.assertTrue(bkr.rgs[0]._provisioning_done)

    @mock.patch('provisioners.beaker.Beaker.release_resources', mock.Mock())
    @mock.patch('time.sleep')
    @mock.patch('builtins.print', mock.Mock())
    @mock.patch('provisioners.beaker.Beaker.update_provisioning_state', mock.Mock())
    def test_wait(self, mock_sleep):
        """Ensure wait works."""
        bkr = ProvisionData.deserialize_file(self.req_asset).beaker

        def succeed_and_fail_intermittently(self, force_recheck, **kwargs):
            data = [False, True, False, False, True]
            succeed_and_fail_intermittently.i += 1
            return data[succeed_and_fail_intermittently.i]

        succeed_and_fail_intermittently.i = -1
        with mock.patch('provisioners.end_conditions.ProvEndConds.evaluate', succeed_and_fail_intermittently):
            result = bkr.wait([], **{'max_aborted_count': 3})
            self.assertEqual(result, RET.PROVISIONING_FAILED)

            mock_sleep.assert_called()

    @mock.patch('provisioners.beaker.Beaker.release_resources')
    @mock.patch('time.sleep')
    @mock.patch('builtins.print')
    @mock.patch('provisioners.beaker.Beaker.update_provisioning_state')
    def test_wait_retcode(self, mock_update, mock_print, mock_sleep, mock_release_resources):
        # pylint: disable=protected-access
        """Ensure wait works and passess retcode around."""
        bkr = ProvisionData.deserialize_file(self.req_asset).beaker

        def fake_evaluate(self, force_recheck, **kwargs):
            self.retcode = kwargs.get('desired_retcode')
            return True

        with mock.patch('provisioners.end_conditions.ProvEndConds.evaluate', fake_evaluate):
            result = bkr.wait([], **{'max_aborted_count': 3, 'desired_retcode': RET.PASSED_TESTING})
            self.assertEqual(result, RET.PASSED_TESTING)

            result = bkr.wait([], **{'max_aborted_count': 3, 'desired_retcode': RET.RUN_USER_ABORTED})
            self.assertEqual(result, RET.RUN_USER_ABORTED)

    def test_get_resource_ids(self):
        """Ensure get_resource_ids works."""
        prov_data = ProvisionData.deserialize_file(self.req_asset)

        self.assertEqual(prov_data.beaker.get_resource_ids(), ['J:1234'])

    @mock.patch('cki_lib.misc.safe_popen')
    @mock.patch('upt.logger.LOGGER.debug')
    @mock.patch('upt.logger.LOGGER.info')
    def test_set_reservation_duration(self, mock_info, mock_debug, mock_safe_popen):
        """Ensure set_reservation_duration works."""
        beaker = ProvisionData.deserialize_file(self.req_asset).beaker
        mock_safe_popen.return_value = ('some output', 'some error', 0)

        beaker.set_reservation_duration(beaker.rgs[0].recipeset.hosts[0])
        mock_debug.assert_called_with('set_reservation_duration error: %s', 'some error')
        mock_info.assert_called_with('set_reservation_duration: %s', 'some output')

        mock_safe_popen.return_value = ('', '', 1)
        with self.assertRaises(RuntimeError):
            beaker.set_reservation_duration(beaker.rgs[0].recipeset.hosts[0])

    @mock.patch('cki_lib.misc.safe_popen')
    @mock.patch('upt.logger.LOGGER.info')
    def test_set_reservation_duration_no_stdout(self, mock_info, mock_safe_popen):
        """Ensure set_reservation_duration doesn't print stdout if there's none."""
        beaker = ProvisionData.deserialize_file(self.req_asset).beaker
        mock_safe_popen.return_value = ('', '', 0)
        beaker.set_reservation_duration(beaker.rgs[0].recipeset.hosts[0])
        mock_info.assert_not_called()

    @mock.patch('cki_lib.misc.safe_popen')
    @mock.patch('builtins.print')
    @mock.patch('time.sleep', lambda *args: None)
    @mock.patch('logging.warning', mock.Mock())
    def test_get_bkr_results(self, mock_print, mock_safe_popen):
        """Ensure get_bkr_results works."""
        beaker = ProvisionData.deserialize_file(self.req_asset).beaker

        mock_safe_popen.return_value = ('some output', '', 1)
        with self.assertRaises(RuntimeError):
            beaker.get_bkr_results('1234')
            mock_print.assert_called_with('some output')

        mock_safe_popen.return_value = ('', '', 0)
        beaker.get_bkr_results('1234')

    @mock.patch('builtins.print')
    def test_get_dead_resource_info(self, mock_print):
        """Ensure get_dead_resource_info works."""
        soup = BS(self.xml, 'xml')
        result_recipesets, erred_rset_ids, ewd_recipe_ids_tasks = soup.findAll('recipeSet'), set(), set()

        result_recipes, _, __ = Beaker.extract_ids(result_recipesets, erred_rset_ids)
        Beaker.get_dead_resource_info(result_recipes, [1], ewd_recipe_ids_tasks)
        recipe_id, task_result = ewd_recipe_ids_tasks.pop()
        self.assertEqual(recipe_id, 1)
        self.assertIsInstance(task_result, TaskResult)
        mock_print.assert_called_with('#1: found EWD in reservesys task!')

        # Ids must be integer compatible.
        with self.assertRaises(ValueError):
            Beaker.get_dead_resource_info([{'id': 'blah'}], [], [])

    @mock.patch('builtins.print')
    @mock.patch('time.sleep')
    def test_quirk_wait(self, mock_sleep, mock_print):
        """Ensure quirk_wait works."""
        beaker = ProvisionData.deserialize_file(self.req_asset).beaker
        with mock.patch.object(beaker, 'get_provisioning_state', lambda *args: (None, None, None)):
            beaker.quirk_wait(beaker.rgs[0])

            mock_print.assert_called_with('* Caught Beaker quirk; waiting 60...', )
            mock_sleep.assert_called_with(60)

    def test_check_beaker_quirk(self):
        """Ensure check_beaker_quirk works."""
        recipe = BS('<recipe status="Aborted" result="New" />', 'xml').find('recipe')
        self.assertTrue(Beaker.check_beaker_quirk([recipe]))

        recipe = BS('<recipe status="Completed" result="New" />', 'xml').find('recipe')
        self.assertFalse(Beaker.check_beaker_quirk([recipe]))

        recipe = BS('<recipe status="Completed" result="New" ><task status="Completed" result="New" />'
                    '<task status="Aborted" result="New" /></recipe>', 'xml').find('recipe')
        self.assertTrue(Beaker.check_beaker_quirk([recipe]))

    def test_heartbeat_no_errors(self):
        """Ensure heartbeat doesn't change sets when there are no errors."""
        beaker = ProvisionData.deserialize_file(self.req_asset).beaker
        with mock.patch.object(beaker, 'get_provisioning_state', lambda *args: (None, None, None)):
            recipe_ids_dead, ewd_recipe_ids_tasks = set(), set()
            beaker.heartbeat(beaker.rgs[0], recipe_ids_dead, ewd_recipe_ids_tasks)
            self.assertEqual(recipe_ids_dead, ewd_recipe_ids_tasks, set())

    @mock.patch('builtins.print')
    @mock.patch('upt.logger.LOGGER.warning', mock.Mock())
    def test_heartbeat_with_errors(self, mock_print):
        """Ensure heartbeat works."""
        # Convert xml in asssets
        beaker = convert_xml(self.xml, **{'exclude': False, 'dev_mode': True, 'force_host_duration': False}).beaker
        soup = BS(self.xml, 'xml')
        # Set recipe_id for each host, which would normally be set by updating provisioning request.
        beaker.rgs[0].recipeset.beaker_hosts[0].recipe_id = 1
        beaker.rgs[0].recipeset.beaker_hosts[1].recipe_id = 2
        recipe_ids_dead = set()

        # Use a fake check_beaker_quirk function to pass on 2nd attempt.
        def fake_quirk(_):
            try:
                if fake_quirk.called:
                    return False
            except AttributeError:
                fake_quirk.called = True
            return True

        result_recipesets, erred_rset_ids, ewd_recipe_ids_tasks = soup.findAll('recipeSet'), {1}, set()
        with mock.patch.object(beaker, 'get_provisioning_state', lambda *args: (None, result_recipesets,
                                                                                erred_rset_ids)):
            result_recipes, erred_recipe_ids, erred_recipes = beaker.extract_ids(result_recipesets, erred_rset_ids)
            # Override erred_recipe_ids to hit an extra safe guard condition.
            erred_recipe_ids = {1}

            with mock.patch.object(beaker, 'extract_ids',
                                   lambda *args: (result_recipes, erred_recipe_ids, erred_recipes)):
                with mock.patch.object(beaker, 'check_beaker_quirk', fake_quirk):
                    with mock.patch.object(beaker, 'quirk_wait', lambda x: (result_recipesets, erred_rset_ids)):
                        beaker.heartbeat(beaker.rgs[0], recipe_ids_dead, ewd_recipe_ids_tasks)
                        recipe_id, task_result = ewd_recipe_ids_tasks.pop()
                        self.assertEqual(recipe_id, 1)
                        self.assertIsInstance(task_result, TaskResult)

        mock_print.assert_called_with('#1: found EWD in reservesys task!')

    def test_is_provisioned(self):
        """Ensure is_provisioned calls get_provisioning_state that returns 3 item tuple for Beaker."""
        beaker = ProvisionData.deserialize_file(self.req_asset).beaker
        with mock.patch.object(beaker, 'get_provisioning_state', lambda *args: (True, None, None)):
            self.assertTrue(beaker.is_provisioned([None]))

    @mock.patch('builtins.print')
    def test_warn_once_provisioning_issue(self, mock_print):
        """Ensure warn_once_provisioning_issue works for recipes."""
        beaker = ProvisionData.deserialize_file(self.req_asset).beaker
        beaker._warned_rsetids = set()
        beaker.warn_once_provisioning_issue(1, [1, 2], 1, 'Warn', '')
        mock_print.assert_called_with('* RS:1 ([1, 2]): R:1 Warn.')

    @mock.patch('builtins.print')
    def test_warn_once_provisioning_issue_reservesys(self, mock_print):
        """Ensure warn_once_provisioning_issue works for reservesys tasks."""
        beaker = ProvisionData.deserialize_file(self.req_asset).beaker
        beaker._warned_rsetids = set()
        beaker.warn_once_provisioning_issue(1, [1, 2], 1, None, 'Warn reservesys')
        mock_print.assert_called_with('* RS:1 ([1, 2]): found reservesys task that Warn reservesys.')

    def test_warn_once_provisioning_issue_unhandled(self):
        """Ensure warn_once_provisioning_issue checks unhandled conditions."""
        beaker = ProvisionData.deserialize_file(self.req_asset).beaker
        beaker._warned_rsetids = set()

        with self.assertRaises(RuntimeError):
            beaker.warn_once_provisioning_issue(0, [], 1, None, None)

    @mock.patch('builtins.print')
    def test_warn_once_provisioning_issue_no_print(self, mock_print):
        """Ensure warn_once_provisioning_issue doesn't print twice."""
        beaker = ProvisionData.deserialize_file(self.req_asset).beaker
        beaker._warned_rsetids = {1}
        beaker.warn_once_provisioning_issue(1, [], 1, 'err', 'err')
        mock_print.assert_not_called()

    @mock.patch('upt.logger.LOGGER.warning')
    def test_delete_failed_hosts(self, mock_warn):
        """Ensure delete_failed_hosts works and leaves only recipes with ids."""
        beaker = convert_xml(self.xml, **{'exclude': False, 'dev_mode': True, 'force_host_duration': False}).beaker
        soup = BS(beaker.rgs[1].recipeset.restraint_xml, 'xml')
        soup.find('recipe')['id'] = 1

        beaker.rgs[1].recipeset.restraint_xml = soup.prettify()

        beaker.delete_failed_hosts(beaker.rgs[1].recipeset)

        self.assertEqual(len(BS(beaker.rgs[1].recipeset.restraint_xml, 'xml').findAll('recipe')), 1)
        mock_warn.assert_called_with('%s removed from XML, no fetch element!', '/distribution/reservesys')

    @mock.patch('upt.logger.LOGGER.warning', mock.Mock())
    def test_update_provisioning_request(self):
        """Ensure update_provisioning_request works."""
        beaker = ProvisionData.deserialize_file(self.req_asset).beaker

        with mock.patch.object(beaker, 'get_bkr_results') as mock_results:
            mock_results.return_value = self.xml

            beaker.update_provisioning_request()

            self.assertIn('<recipe id="1"', beaker.rgs[0].recipeset.restraint_xml)

    def test_resource_group2xml(self):
        """Ensure resource_group2xml works."""
        beaker = ProvisionData.deserialize_file(self.req_asset).beaker
        result = beaker.resource_group2xml(beaker.rgs[0])
        result = BS(result, 'xml')

        self.assertIsNotNone(result.find('job'))
        self.assertIsNotNone(result.find('recipeSet'))

    @mock.patch('upt.logger.LOGGER.warning', mock.Mock())
    def test_preprovision_disabled(self):
        """Ensure disabled preprovisioning doesn't return any new xmls or resource groups."""
        beaker = ProvisionData.deserialize_file(self.req_asset).beaker
        new_xmls, new_resource_groups = beaker.preprovision(**{'preprovision': None, 'dev_mode': True})
        self.assertEqual(new_xmls, new_resource_groups, [])

    def test_preprovision(self):
        """Ensure preprovision works."""
        beaker = ProvisionData.deserialize_file(self.req_asset).beaker
        beaker.rgs.append(copy.deepcopy(beaker.rgs[0]))
        new_xmls, new_resource_groups = beaker.preprovision(**{'preprovision': '1', 'dev_mode': True})

        self.assertEqual(len(new_xmls), 1)
        self.assertEqual(len(new_resource_groups), 1)

    def test_provision_preprovison(self):
        """Ensure provision works with preprovisioning."""
        beaker = ProvisionData.deserialize_file(self.req_asset).beaker

        with mock.patch.object(beaker, 'submit_job') as mock_submit:
            mock_submit.side_effect = [['J:1234'], ['J:5678']]
            all_resource_ids = beaker.provision(**{'preprovision': '0', 'dev_mode': True})

        self.assertEqual(all_resource_ids, ['J:1234', 'J:5678'])

    def test_provision(self):
        """Ensure provision works."""
        beaker = ProvisionData.deserialize_file(self.req_asset).beaker

        with mock.patch.object(beaker, 'submit_job') as mock_submit:
            mock_submit.side_effect = [['J:1234']]
            all_resource_ids = beaker.provision(**{'preprovision': None, 'dev_mode': True})

        self.assertEqual(all_resource_ids, ['J:1234'])

    def test_set_harness(self):
        """Ensure set_harness works."""
        soup = BS('''<recipe ks_meta="harness='restraint-rhts beakerlib'" />''', 'xml')
        Beaker.set_harness(soup)
        self.assertIn('''<recipe ks_meta="harness='restraint-rhts beakerlib'"/>''', soup.prettify())

        soup = BS('''<recipe ks_meta="" />''', 'xml')
        Beaker.set_harness(soup)
        self.assertIn('''<recipe ks_meta="harness='restraint-rhts beakerlib'"/>''', soup.prettify())

    def test_finalize_recipes(self):
        """Ensure _finalize_recipes works."""
        beaker = ProvisionData.deserialize_file(self.req_asset).beaker

        # Use any valid xml to see that  reservesys is added.
        soup = BS(beaker.rgs[0].recipeset.restraint_xml, 'xml')
        self.assertNotIn('/distribution/reservesys', soup.prettify())
        result = beaker._finalize_recipes(soup, beaker.rgs[0].recipeset)
        self.assertIn('/distribution/reservesys', result)

    @mock.patch('cki_lib.misc.safe_popen')
    @mock.patch('builtins.print')
    def test_submit_job(self, mock_print, mock_safe_popen):
        """Ensure submit_job works."""
        stdout = 'J:1234 J:5678'
        mock_safe_popen.return_value = (stdout, '', 0)
        resource_ids = Beaker.submit_job('whatever')

        self.assertEqual(resource_ids, stdout.split(' '))

        mock_print.assert_called_with(f'Submitted: {" ".join(resource_ids)}')

    @mock.patch('cki_lib.misc.safe_popen')
    @mock.patch('builtins.print')
    def test_submit_job_err(self, mock_print, mock_safe_popen):
        """Ensure submit_job raises exception and prints stdout on non-zero retcode."""

        mock_safe_popen.return_value = ('stdout', '', 1)
        with self.assertRaises(RuntimeError):
            Beaker.submit_job('whatever')

        mock_print.assert_called_with('stdout')

    @mock.patch('cki_lib.misc.safe_popen')
    def test_clone(self, mock_safe_popen):
        """Ensure clone works."""
        stdout = 'J:1234 J:5678'
        mock_safe_popen.return_value = (stdout, '', 0)
        resource_ids = Beaker.clone('whatever')

        self.assertEqual(resource_ids, stdout.split(' '))

    @mock.patch('cki_lib.misc.safe_popen')
    @mock.patch('builtins.print')
    def test_clone_err(self, mock_print, mock_safe_popen):
        """Ensure clone raises exception and prints stdout on non-zero retcode."""

        mock_safe_popen.return_value = ('stdout', '', 1)
        with self.assertRaises(RuntimeError):
            Beaker.clone('whatever')

        mock_print.assert_called_with('stdout')

    @mock.patch('cki_lib.misc.safe_popen')
    @mock.patch('builtins.print')
    def test_cancel(self, mock_print, mock_safe_popen):
        # pylint: disable=no-self-use
        """Ensure cancel works."""

        mock_safe_popen.return_value = ('stdout', '', 0)
        Beaker.cancel('whatever')

        mock_print.assert_called_with('stdout')

    @mock.patch('cki_lib.misc.safe_popen')
    @mock.patch('builtins.print')
    def test_cancel_raise_on_error(self, mock_print, mock_safe_popen):
        """Ensure cancel works."""

        mock_safe_popen.return_value = ('stdout', '', 1)
        with self.assertRaises(RuntimeError):
            Beaker.cancel('whatever')

        mock_print.assert_called_with('stdout')

    @mock.patch('builtins.print')
    def test_reprovision_aborted(self, mock_print):
        """Ensure reprovision_aborted works."""
        beaker = ProvisionData.deserialize_file(self.req_asset).beaker
        beaker.rgs[0]._erred_rset_ids = {1}

        with mock.patch.object(beaker, 'cancel'):
            with mock.patch.object(beaker, 'get_bkr_results') as fake_results:
                fake_results.return_value = pathlib.Path(self.req_asset).read_text()
                with mock.patch.object(beaker, 'clone') as fake_clone:
                    fake_clone.return_value = ['J:1234']

                    beaker.reprovision_aborted()
                    mock_print.assert_called_with('* Cloned RS:1 as J:1234')

    def test_release_resources(self):
        """Ensure release_resources works."""
        beaker = ProvisionData.deserialize_file(self.req_asset).beaker
        with mock.patch.object(beaker, 'cancel') as mock_cancel:
            beaker.release_resources()
            mock_cancel.assert_called_with('J:1234')

    @mock.patch('builtins.print')
    def test_get_provisioning_state(self, mock_print):
        """Ensure get_provisioning_state works."""
        beaker = ProvisionData.deserialize_file(self.req_asset).beaker

        with mock.patch.object(beaker, 'get_bkr_results') as fake_results:
            fake_results.return_value = self.xml
            provisioned, recipesets, erred_rset_ids = beaker.get_provisioning_state(beaker.rgs[0])
            self.assertFalse(provisioned)
            self.assertEqual(len(recipesets), 3)
            self.assertEqual(erred_rset_ids, {2, 3})

            mock_print.assert_any_call("* RS:2 (['2', '3', '4', '0']): R:0 Aborted.")
            mock_print.assert_any_call("* RS:3 (['6']): R:6 Aborted.")

    @mock.patch('builtins.print', mock.Mock())
    @mock.patch('upt.logger.LOGGER.warning')
    def test_update_provisioning_request_inconsistency(self, mock_warning):
        """Ensure update_provisioning_request works, test for inconsistency of new bkr-results and internal data."""
        beaker = ProvisionData.deserialize_file(self.req_asset).beaker

        with mock.patch.object(beaker, 'get_bkr_results') as fake_results:
            fake_results.return_value = self.xml
            beaker.rgs[0]._erred_rset_ids = {1}
            beaker.update_provisioning_request()
            assert mock_warning.call_count == 2, "Invalid call count"

            beaker.rgs[0]._erred_rset_ids = set()
            beaker.update_provisioning_request()
            mock_warning.assert_any_call('Error: a system went down shortly after provisioning was completed!')
