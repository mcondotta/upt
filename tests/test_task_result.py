"""Test cases for task_result module."""
import unittest
from bs4 import BeautifulSoup as BS

from restraint_wrap.task_result import TaskResult


class TestTaskResult(unittest.TestCase):
    """Test cases for task_result module."""

    def setUp(self) -> None:
        self.tasks = [BS('<task id="1" name="a3" status="Completed" result="Pass"/>', 'xml').find('task')]

    def test_init(self):
        """Ensure TaskResult is created OK."""

        task_result = TaskResult('1234', self.tasks[0], 1, 'Pass', 'Completed')

        self.assertEqual(task_result.recipe_id, 1234)

    def test_task2task_results(self):
        """Ensure task2task_results works."""
        TaskResult.task2task_results(1234, self.tasks)
