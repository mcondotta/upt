"""Test cases for console module."""
import itertools
import pathlib
import tempfile

import unittest
from unittest.mock import patch

from twisted.internet.error import ProcessExitedAlready
from twisted.python.failure import Failure

from cki_lib.misc import tempfile_from_string

from restraint_wrap.console import ConsoleProtocol
from restraint_wrap.restraint_host import RestraintHost


# pylint: disable=no-member


class TestConsole(unittest.TestCase):
    """Testcase for Console."""

    def _do_run(self, tempdir):
        path = pathlib.Path(tempdir, 'run.01/recipes/1234')
        self.kwargs['output'] = tempdir
        path.mkdir(exist_ok=True, parents=True)

        with patch('restraint_wrap.console.ConsoleProtocol.binary', 'echo'):
            console = ConsoleProtocol(self.host, **self.kwargs)

        return console

    def setUp(self) -> None:
        """Prepare for testing."""
        self.host = RestraintHost('bxe.hostname1', 1234)
        self.kwargs = {'output': '/tmp/',
                       'run_path_suffix': 'run.01',
                       'conf_cons': None}

    def test_console_server_set(self):
        """Ensure that matching server for hostname set."""

        with tempfile_from_string(b'bxe: server1.com\n') as fname:
            with tempfile.TemporaryDirectory() as tempdir:
                self.kwargs['output'] = tempdir
                self.kwargs['conf_cons'] = fname
                console = self._do_run(tempdir)

                # hostname matches prefix, server is set
                self.assertEqual(console.server, 'server1.com')

    def test_init2(self):
        """Test that console starts process OK."""

        with tempfile_from_string(b'donotmatch: server1.com\n') as fname:
            with tempfile.TemporaryDirectory() as tempdir:
                self.kwargs['output'] = tempdir
                self.kwargs['conf_cons'] = fname
                console = self._do_run(tempdir)
                self.assertEqual(console.server, 'console.lab.bos.redhat.com')

    @patch('upt.logger.LOGGER.info')
    def test_console_data(self, mock_logging):
        """Test that console stdout/stderr handler processes data OK without breaking."""
        with tempfile.TemporaryDirectory() as tempdir:
            self.kwargs['output'] = tempdir
            console = self._do_run(tempdir)
            console.childDataReceived(1, b'')
            console.childDataReceived(1, b'somedata\n')
            self.assertEqual(console.stdout, b'somedata\n')

            # Data is processed.
            stdout_data = b'-- Console down'
            console.childDataReceived(1, stdout_data)
            self.assertIn(stdout_data, console.stdout)

            stderr_data = b'stderr output'
            console.childDataReceived(2, b'')
            console.childDataReceived(2, stderr_data)
            self.assertEqual(console.stderr, stderr_data)

            self.assertEqual(console.server, 'console.lab.bos.redhat.com')

        # Protocol noticed console state change
        mock_logging.assert_called()

    def test_terminate(self):
        """Ensure save_and_terminate works and files are written."""
        with tempfile.TemporaryDirectory() as tempdir:
            console = self._do_run(tempdir)
            console.childDataReceived(1, b'stdout')
            console.childDataReceived(2, b'stderr')
            console.save_and_terminate()

            with open(console.stderr_path) as fhandle:
                self.assertEqual('stderr', fhandle.read())

            with open(console.stdout_path) as fhandle:
                self.assertEqual('stdout', fhandle.read())

    def test_terminate_no_stderr(self):
        """Ensure no stderr file is created when there's no stderr data."""
        with tempfile.TemporaryDirectory() as tempdir:
            console = self._do_run(tempdir)
            console.save_and_terminate()

            self.assertFalse(pathlib.Path(console.stderr_path).is_file())

            # Ensure nothing happens afterwards.
            self.assertTrue(console.finished)
            console.save_and_terminate()

    def test_terminate_process(self):
        """Ensure processEnded call doesn't break anything."""
        with tempfile.TemporaryDirectory() as tempdir:
            console = self._do_run(tempdir)
            console.processEnded(Failure(RuntimeError('errhm')))

    def test_termination_exception(self):
        """Ensure console doesn't break anything if the process already terminated."""
        with tempfile.TemporaryDirectory() as tempdir:
            console = self._do_run(tempdir)
            with patch.object(console, 'proc') as mock_proc:
                mock_proc.signalProcess.side_effect = itertools.chain(
                    [ProcessExitedAlready()], itertools.cycle([None]))

                console.save_and_terminate()
