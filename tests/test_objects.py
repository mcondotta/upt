"""Test cases for provisioners/objects module."""
import os
import unittest

from provisioners.format import ProvisionData
from provisioners.objects import Host
from tests.const import ASSETS_DIR


class TestObjects(unittest.TestCase):
    """Test cases for objects module."""

    def setUp(self) -> None:
        self.req_asset = os.path.join(ASSETS_DIR, 'req.yaml')

    def test_hosts_method(self):
        """Ensure .hosts property returns all (3) hosts of our asset file."""
        prov_data = ProvisionData.deserialize_file(self.req_asset)
        results = prov_data.beaker.rgs[0].recipeset.hosts
        self.assertIsInstance(results[0], Host)
        self.assertEqual(len(results), 3)

    def test_hosts_method_raise(self):
        """Ensure .hosts property returns all (1) hosts of our asset file."""
        prov_data = ProvisionData({'beaker': {'rgs': [{'recipeset': {'beaker_hosts': [], 'restraint_xml': ''}}]}})
        with self.assertRaises(RuntimeError):
            _ = prov_data.beaker.rgs[0].recipeset.hosts

    def test_rg_aws_hosts(self):
        """Ensure aws_hosts works."""
        tmp = ProvisionData({'aws': {'rgs': [{'recipeset': {'aws_hosts': [], 'restraint_xml': ''}}]}})
        tmp.aws.rgs[0].recipeset.aws_hosts.append(Host())
        tmp.aws.rgs[0].recipeset.aws_hosts.append(Host())
        self.assertEqual(len(list(tmp.aws.aws_hosts())), 2)
