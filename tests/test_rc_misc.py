"""Test cases for restraint_wrap/misc module."""
import os
import unittest
import tempfile
from restraint_wrap import misc


class TestRCMisc(unittest.TestCase):
    """Test cases for restraint_wrap/misc module."""

    def test_add_directory_suffix(self):
        """Ensure add_directory_suffix works."""
        self.assertEqual(misc.add_directory_suffix('tests'), os.path.abspath('tests') + '.done')

        with tempfile.TemporaryDirectory() as tdir:
            new_dir = os.path.join(tdir, 'dir.done')
            os.makedirs(new_dir)
            # When new_dir exists, the function will generate name with .01 suffix
            self.assertEqual(new_dir + '.01', misc.add_directory_suffix(tdir + '/dir'))
