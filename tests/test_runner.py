"""Test cases for restraint_wrap/runner module."""
import itertools
import os
import unittest
from unittest import mock

from twisted.internet.error import ReactorNotRunning

from provisioners.format import ProvisionData
from provisioners.objects import ResourceGroup, Host
from restraint_wrap.restraint_protocol import RestraintClientProcessProtocol
from restraint_wrap.runner import RunnerGlue
from tests.const import ASSETS_DIR
from upt.misc import RET


class TestRunner(unittest.TestCase):
    """Test cases for runner module."""

    def tearDown(self) -> None:
        self.mock_prov.stop()

    @mock.patch('restraint_wrap.restraint_protocol.Watcher', mock.Mock())
    def setUp(self) -> None:
        self.std_kwargs = {'keycheck': 'no', 'dev_mode': True, 'reruns': 3, 'console': False, 'output': '/tmp/a',
                           'input': None}
        self.req_asset = os.path.join(ASSETS_DIR, 'req.yaml')

        self.provision_data = ProvisionData.deserialize_file(self.req_asset)
        self.beaker = self.provision_data.beaker

        self.mock_prov = mock.patch('restraint_wrap.runner.ProvisionData.deserialize_file',
                                    lambda *x: self.provision_data)
        self.mock_prov.start()

    @mock.patch('restraint_wrap.runner.RunnerGlue.add_protocol')
    @mock.patch('builtins.print', mock.Mock())
    def test_init(self, mock_add_protocol):
        """Ensure runner creates protocols and check preprovisioned rgs."""
        with mock.patch.object(self.beaker.rgs[0], 'preprovisioned', True):
            RunnerGlue(**self.std_kwargs)
            mock_add_protocol.assert_not_called()

    @mock.patch('restraint_wrap.runner.RunnerGlue.add_protocol')
    @mock.patch('builtins.print', mock.Mock())
    def test_init_no_preprovision(self, mock_add_protocol):
        """Ensure runner creates protocols."""
        with mock.patch.object(self.provision_data.beaker.rgs[0], 'preprovisioned', False):
            RunnerGlue(**self.std_kwargs)
            mock_add_protocol.assert_called()

    @mock.patch('restraint_wrap.runner.RunnerGlue.add_protocol', mock.Mock())
    @mock.patch('upt.logger.LOGGER.debug')
    def test_start_protocols(self, mock_debug):
        """Ensure start_protocols works."""
        glue = RunnerGlue(**self.std_kwargs)
        mock1 = mock.MagicMock()
        mock1.proc = None

        mock2 = mock.MagicMock()

        mock3 = mock.MagicMock()
        mock3.proc = 'process-created'
        mock3.run_path_suffix = ''

        with mock.patch.object(glue, 'protocols', []):
            glue.start_protocols()
            mock_debug.assert_not_called()

        with mock.patch.object(glue, 'protocols', [mock1]):
            glue.start_protocols()
            mock_debug.assert_called_with('Starting protocol for resource_id %s...', mock1.resource_group.resource_id)
            mock1.start_all.assert_called()

        with mock.patch.object(glue, 'protocols', [mock2]):
            glue.start_protocols()
            mock2.start_all.assert_not_called()

        with mock.patch.object(glue, 'protocols', [mock3]):
            glue.start_protocols()
            mock3.start_all.assert_not_called()

    @mock.patch('restraint_wrap.runner.RunnerGlue.add_protocol', mock.Mock())
    @mock.patch('restraint_wrap.runner.reactor')
    @mock.patch('restraint_wrap.runner.RunnerGlue.start_protocols')
    def test_wait_on_protocols(self, mock_start_protocols, mock_reactor):
        """Ensure wait_on_protocols works."""
        glue = RunnerGlue(**self.std_kwargs)

        mock_reactor.running = True
        with mock.patch('provisioners.interface.ProvisionerCore.all_recipes_finished', lambda *x: False):
            glue.wait_on_protocols()
            mock_reactor.callFromThread.assert_not_called()
            assert mock_start_protocols.call_count == 1

        with mock.patch('provisioners.interface.ProvisionerCore.all_recipes_finished', lambda *x: True):
            with mock.patch.object(glue, 'protocols', [None]):
                mock_reactor.running = False
                glue.wait_on_protocols()
                mock_reactor.callFromThread.assert_not_called()
                assert mock_start_protocols.call_count == 2

                mock_reactor.running = True
                glue.wait_on_protocols()
                assert mock_start_protocols.call_count == 3
                mock_reactor.callFromThread.assert_called()

                mock_reactor.callFromThread.side_effect = itertools.chain([ReactorNotRunning()],
                                                                          itertools.cycle([None]))
                glue.wait_on_protocols()

    def test_add_protocol(self):
        """Ensure add_protocol works."""
        with mock.patch('restraint_wrap.runner.RunnerGlue.add_protocol', mock.Mock()):
            glue = RunnerGlue(**self.std_kwargs)

        with mock.patch.object(self.beaker, 'set_reservation_duration'):
            with mock.patch('restraint_wrap.restraint_protocol.ShellWrap'):
                with mock.patch('restraint_wrap.restraint_protocol.RestraintHost'):
                    result = glue.add_protocol(self.beaker, self.beaker.rgs[0], **self.std_kwargs)

        self.assertIsInstance(result, RestraintClientProcessProtocol)

    @mock.patch('restraint_wrap.runner.RunnerGlue.add_protocol', mock.Mock())
    @mock.patch('restraint_wrap.runner.reactor')
    @mock.patch('restraint_wrap.runner.task')
    @mock.patch('builtins.print')
    def test_run(self, mock_print, mock_task, mock_reactor):
        """Ensure run works."""
        glue = RunnerGlue(**self.std_kwargs)

        retcode = glue.run()

        mock_reactor.addSystemEventTrigger.assert_called_with('before', 'shutdown', glue.cleanup_handler)
        mock_print.assert_called_with('Runner waiting for processes to finish...')
        # Default retcode is set.
        self.assertEqual(retcode, RET.TOO_MANY_ERRORS)

        mock_task.LoopingCall.assert_called_with(glue.wait_on_protocols)

    @mock.patch('restraint_wrap.runner.RunnerGlue.add_protocol', mock.Mock())
    @mock.patch('upt.logger.LOGGER.error')
    def test_handle_runner_failure(self, mock_error):
        """Ensure handle_runner_failure works."""
        glue = RunnerGlue(**self.std_kwargs)
        fail_mock = mock.Mock()
        glue.handle_runner_failure(fail_mock)

        mock_error.assert_called_with('FAILURE in deferred: %s', fail_mock)

    @mock.patch('restraint_wrap.runner.RunnerGlue.add_protocol', mock.Mock())
    @mock.patch('upt.logger.LOGGER.debug')
    @mock.patch('upt.logger.LOGGER.error')
    @mock.patch('builtins.print')
    def test_cleanup_handler(self, mock_print, mock_error, mock_debug):
        # pylint: disable=no-member,protected-access
        """Ensure cleanup_handler works."""
        glue = RunnerGlue(**self.std_kwargs)

        mock0 = mock.MagicMock()
        mock1 = mock.MagicMock()
        mock2 = mock.MagicMock()
        with mock.patch.object(glue, 'protocols', [mock0, mock1, mock2]):
            with mock.patch.object(self.beaker, 'release_resources'):
                mock0.proc = None

                mock1.extra_retcodes = [RET.PASSED_TESTING]
                mock2.extra_retcodes = [RET.RESTRAINT_PROBLEM, RET.CMD_LINE_ARGS]

                mock1.proc.status = 'whatever'
                mock1.proc._getReason.return_value = 'ended'

                mock2.proc.status = 'whatever'
                mock2.proc._getReason.return_value = 'ended by signal 2'

                glue.cleanup_handler()

                mock_print.assert_called_with('* Abnormal exit!')
                mock_debug.assert_any_call('* Runner cleanup handler runs...')
                mock_error.assert_called_with('\n*** %s ***', RET.TOO_MANY_ERRORS)
                self.assertTrue(glue.cleanup_done)

    @mock.patch('restraint_wrap.runner.RunnerGlue.add_protocol', mock.Mock())
    @mock.patch('builtins.print')
    def test_cleanup_handler_pass(self, mock_print):
        """Ensure cleanup_handler works and that it can release resources according to cmd-line args."""
        kwargs = {'keycheck': 'no', 'dev_mode': False, 'reruns': 3, 'output': '/tmp/a', 'input': None}
        glue = RunnerGlue(**kwargs)

        mock0 = mock.MagicMock()
        with mock.patch.object(glue.provisioners[0].rgs[0].recipeset.hosts[1], '_done_processing', True):
            with mock.patch.object(glue, 'protocols', [mock0]):
                with mock.patch.object(glue, 'eval_retcodes', lambda *x: RET.PASSED_TESTING):
                    with mock.patch.object(self.beaker, 'release_resources') as mock_release_resources:
                        glue.cleanup_handler()
                        mock_print.assert_any_call('* Kernel PASSED testing!')
                        mock_print.assert_any_call('* Abnormal exit!')
                        mock_release_resources.assert_called()

    @mock.patch('restraint_wrap.runner.RunnerGlue.add_protocol', mock.Mock())
    @mock.patch('builtins.print')
    def test_cleanup_handler_normal_exit(self, mock_print):
        # pylint: disable=protected-access
        """Ensure cleanup_handler works and that 'abnormal exit' warning isn't printed when all hosts finished."""
        kwargs = {'keycheck': 'no', 'dev_mode': False, 'reruns': 3, 'output': '/tmp/a', 'input': None}

        mock0 = mock.MagicMock()

        glue = RunnerGlue(**kwargs)

        host = Host()
        host._done_processing = True
        with mock.patch.object(glue.provisioners[0].rgs[0].recipeset, 'beaker_hosts', [host]):
            with mock.patch.object(glue, 'protocols', [mock0]):
                with mock.patch.object(glue, 'eval_retcodes', lambda *x: RET.PASSED_TESTING):
                    with mock.patch.object(self.beaker, 'release_resources') as mock_release_resources:
                        glue.cleanup_handler()
                        mock_print.assert_called_with('* Kernel PASSED testing!')
                        mock_release_resources.assert_called()

    @mock.patch('restraint_wrap.runner.RunnerGlue.add_protocol', mock.Mock())
    @mock.patch('upt.logger.LOGGER.debug')
    def test_cleanup_handler_done(self, mock_debug):
        """Ensure cleanup_handler works."""
        glue = RunnerGlue(**self.std_kwargs)
        glue.cleanup_done = True

        glue.cleanup_handler()

        mock_debug.assert_not_called()

    @mock.patch('builtins.print')
    def test_eval_retcodes(self, mock_print):
        """Ensure eval_retcodes works."""
        rg_preprovisioned = ResourceGroup()
        rg_preprovisioned.preprovisioned = True

        with mock.patch('provisioners.objects.RecipeSet.hosts', []):
            RunnerGlue.eval_retcodes([rg_preprovisioned, ResourceGroup()], [])

        mock_print.assert_called()

        self.assertEqual(RET.RESTRAINT_PROBLEM, RunnerGlue.eval_retcodes([], [RET.RESTRAINT_PROBLEM]))
        self.assertEqual(RET.RUN_USER_ABORTED, RunnerGlue.eval_retcodes([], [RET.RUN_USER_ABORTED]))
        self.assertEqual(RET.PASSED_TESTING, RunnerGlue.eval_retcodes([], [RET.PASSED_TESTING]))
        self.assertEqual(RET.FAILED_TESTING, RunnerGlue.eval_retcodes([], [RET.FAILED_TESTING, RET.PASSED_TESTING]))
        self.assertEqual(RET.CMD_LINE_ARGS, RunnerGlue.eval_retcodes([], [RET.PASSED_TESTING, RET.CMD_LINE_ARGS]))
        self.assertEqual(RET.INFRASTRUCTURE_ISSUE,
                         RunnerGlue.eval_retcodes([], [RET.INFRASTRUCTURE_ISSUE, RET.CMD_LINE_ARGS]))

        self.assertEqual(RET.PROVISIONING_FAILED,
                         RunnerGlue.eval_retcodes([], [RET.PROVISIONING_FAILED, RET.PASSED_TESTING,
                                                       RET.RESOURCE_IS_DEAD, RET.TOO_MANY_ERRORS]))
