"""Test cases for RestraintWrap __main__ module."""
import unittest
import tempfile
from unittest import mock

from click.testing import CliRunner

from cki_lib.misc import tempfile_from_string
from restraint_wrap.__main__ import cli, main
from upt.misc import RET

from tests.const import FakeProv


class TestRestraintWrap(unittest.TestCase):
    """Test cases for RestraintWrap __main__ module."""

    @mock.patch('builtins.print', mock.Mock())
    def test_test_api(self):
        # pylint: disable=no-self-use
        """Test that click api (test) executes."""
        runner = CliRunner()

        with tempfile.TemporaryDirectory() as tempdir:
            with tempfile_from_string(b'') as rcfile:
                with tempfile_from_string(b'') as ignore:
                    response = runner.invoke(cli, ['test', '--rc', rcfile, '-i', ignore, '-o', tempdir])
                    self.assertEqual(response.exit_code, 1)

    @mock.patch('provisioners.format.ProvisionData.provisioners', new=lambda x: [FakeProv()])
    @mock.patch('restraint_wrap.runner.RunnerGlue.run', lambda x: RET.PASSED_TESTING)
    def test_test_api2(self):
        # pylint: disable=no-self-use
        """Test that click api (test) executes."""
        runner = CliRunner()

        with tempfile.TemporaryDirectory() as tempdir:
            with tempfile_from_string(b'') as rcfile:
                with tempfile_from_string(b'') as ignore:
                    response = runner.invoke(cli, ['test', '--rc', rcfile, '-i', ignore, '--no-dev-mode',
                                                   '--conf-cons', '', '-o', tempdir])
                    self.assertEqual(response.exit_code, 0)

    @mock.patch('restraint_wrap.__main__.cli')
    def test_restraint_main_method(self, mock_cli):
        # pylint: disable=no-self-use
        """Ensure main works."""

        main()
