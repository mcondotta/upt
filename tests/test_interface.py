"""Test cases for provisioner interface module."""
import copy
import os
import unittest

from provisioners.format import ProvisionData
from provisioners.objects import ResourceGroup, RecipeSet, Host

from tests.const import ASSETS_DIR


class TestProvisionerCore(unittest.TestCase):
    """Test cases for interface module."""

    def setUp(self) -> None:
        self.req_asset = os.path.join(ASSETS_DIR, 'req.yaml')
        prov_data = ProvisionData.deserialize_file(self.req_asset)
        self.bkr = prov_data.beaker

    def test_find_objects_rgs(self):
        """Ensure find_objects works by getting ResourceGroups."""
        # return objects that are instances of ResourceGroup class
        result = self.bkr.find_objects(self.bkr.rgs, lambda x: x if isinstance(x, ResourceGroup) else None)
        self.assertEqual(self.bkr.rgs, result)

    def test_find_objects_recipests(self):
        """Ensure find_objects works by getting RecipeSets."""
        # return objects that are instances of RecipeSet class
        result = self.bkr.find_objects(self.bkr.rgs, lambda x: x if isinstance(x, RecipeSet) else None)
        self.assertEqual([self.bkr.rgs[0].recipeset], result)

    def test_find_objects_hosts(self):
        """Ensure find_objects works by getting Hosts."""
        # return objects that are instances of Host class
        result = self.bkr.find_objects(self.bkr.rgs, lambda x: x if isinstance(x, Host) else None)
        self.assertEqual(self.bkr.rgs[0].recipeset.hosts, result)

    def test_get_all_hosts(self):
        """Ensure get_all_hosts works."""
        result = self.bkr.get_all_hosts(self.bkr.rgs)
        self.assertEqual(self.bkr.rgs[0].recipeset.hosts, result)

    def test_find_host_object(self):
        """Ensure find_host_object works."""
        first_host = self.bkr.find_host_object(123)
        self.assertEqual(self.bkr.rgs[0].recipeset.hosts[0], first_host)

        second_host = self.bkr.find_host_object(456)
        self.assertEqual(self.bkr.rgs[0].recipeset.hosts[1], second_host)

    def test_all_recipes_finished(self):
        """Ensure all_recipes_finished works."""
        result = self.bkr.all_recipes_finished(self.bkr.rgs)
        self.assertFalse(result)

    def test_all_recipes_finished_true(self):
        # pylint: disable=protected-access
        """Ensure all_recipes_finished works by setting host as 'done processing'."""
        bkr = copy.deepcopy(self.bkr)
        for host in bkr.rgs[0].recipeset.hosts:
            host._done_processing = True

        result = bkr.all_recipes_finished(bkr.rgs)
        self.assertTrue(result)
