"""Test cases for restraint_wrap/restraint_protocol.py module."""
import copy
import os
import itertools
import pathlib
import shlex
import unittest
from unittest import mock

from twisted.internet.error import ReactorNotRunning, ProcessExitedAlready
from bs4 import BeautifulSoup as BS

from provisioners.format import ProvisionData
from upt.misc import RET
from restraint_wrap.actions import ActionOnResult
from restraint_wrap.restraint_protocol import RestraintClientProcessProtocol
from restraint_wrap.restraint_host import RestraintHost
from restraint_wrap.task_result import TaskResult
from tests.const import ASSETS_DIR


# pylint: disable=no-self-use,protected-access

class TestRestraintProtocol(unittest.TestCase):
    # pylint: disable=too-many-instance-attributes,too-many-public-methods
    """Ensure RestraintClientProcessProtocol works."""

    @mock.patch('restraint_wrap.restraint_protocol.Watcher', mock.Mock())
    def setUp(self) -> None:
        self.std_kwargs = {'keycheck': 'no', 'dev_mode': True, 'reruns': 3, 'console': False, 'output': '/tmp/a'}
        self.req_asset = os.path.join(ASSETS_DIR, 'req.yaml')

        self.xmlpath = pathlib.Path(ASSETS_DIR, 'beaker_xml')
        self.xml = self.xmlpath.read_text()

        self.mock_tempdir = mock.patch('restraint_wrap.restraint_shell.tempfile.TemporaryDirectory')
        self.mock_tempdir.start()

        self.mock_rs_open = mock.patch('restraint_wrap.restraint_shell.open', create=True)
        self.mock_rs_open.start()

        self.beaker = ProvisionData.deserialize_file(self.req_asset).beaker
        kwargs = {'keycheck': 'no', 'dev_mode': True, 'reruns': 3, 'output': '/tmp/a', 'console': False}
        self.proto = RestraintClientProcessProtocol(self.beaker, self.beaker.rgs[0], lambda *args: None, **kwargs)

    def tearDown(self) -> None:
        self.mock_tempdir.stop()
        self.mock_rs_open.stop()

    @mock.patch('upt.logger.LOGGER.error')
    def test_get_restraint_xml_task(self, mock_error):
        """Ensure get_restraint_xml_task works."""
        proto = self.proto
        soup = proto.wrap.run_soup = BS(self.xml, 'xml')

        # Simple extraction of a task from xml.
        result = proto.get_restraint_xml_task(1, 1)
        self.assertEqual(soup.findAll('task')[0], result)

        # Task that doesn't exist -> None + error print.
        self.assertIsNone(proto.get_restraint_xml_task(1, 5000))
        mock_error.assert_called_with('task T:%i not found in recipe %s!', 5000, 1)

    @mock.patch('builtins.print', mock.Mock())
    @mock.patch('restraint_wrap.restraint_protocol.reactor')
    def test_start_all(self, mock_reactor):
        """Ensure start_all calls what it should."""
        proto = self.proto
        mock_reactor.spawnProcess.return_value = 'spawned'
        mock_reactor.addSystemEventTrigger.return_value = 'trigger-set'

        proto.start_all()

        mock_reactor.spawnProcess.assert_called_with(proto, proto.restraint_binary,
                                                     shlex.split(proto.wrap.restraint_commands),
                                                     env=os.environ)
        mock_reactor.addSystemEventTrigger.assert_called()

    @mock.patch('restraint_wrap.watcher.Semaphore', mock.Mock())
    def test_filter_output(self):
        """Ensure filter_output works."""
        # Check that identify_host extracts hostname + recipe_id.
        self.assertIsNone(self.proto.filter_output('Connecting to host: root@hostname1, recipe id:123'))
        rst_host = self.proto.rst_hosts[0]
        self.assertEqual(rst_host.hostname, 'hostname1')
        self.assertEqual(rst_host.recipe_id, 123)

        self.assertIsNone(self.proto.filter_output('Connecting to host: root@a.b.com, recipe id:123'))

    def test_identify_host(self):
        """Ensure identify_host works."""
        self.assertTrue(self.proto.identify_host('Connecting to host: root@a.b.com, recipe id:1234'))
        self.assertFalse(self.proto.identify_host('invalid line'))

    def test_start_consoles(self):
        """Ensure start_consoles works."""
        with mock.patch('restraint_wrap.restraint_protocol.ConsoleProtocol'):
            # We need modified object, make a copy so we don't mess up other tests.
            proto = copy.deepcopy(self.proto)
            proto.kwargs['console'] = True

            proto.start_consoles()
            self.assertEqual(len(proto.consoles), 3)

    def test_start_no_consoles(self):
        """Ensure start_consoles doesn't start any console with non-Beaker provisioner or consoles are disabled."""
        self.proto.start_consoles()
        self.assertEqual(self.proto.consoles, [])

    @mock.patch('upt.logger.LOGGER.debug')
    @mock.patch('restraint_wrap.restraint_protocol.twisted_task')
    @mock.patch('restraint_wrap.restraint_protocol.defer')
    def test_out_received(self, mock_defer, mock_task, mock_debug):
        """Ensure outReceived works."""
        with mock.patch.object(self.proto, 'find_host_lines') as mock_find:
            self.proto.outReceived(b'some data')
            mock_debug.assert_called_with('some data')

            mock_task.LoopingCall.assert_called_with(self.proto.host_hearbeat)
            mock_find.assert_called_with('some data')
            mock_defer.Deferred.assert_called()

            self.proto.outReceived(b'')

    @mock.patch('upt.logger.LOGGER.error')
    def test_handle_failure(self, mock_error):
        """Ensure handle_failure works."""
        mock_failure = mock.Mock()

        with mock.patch.object(self.proto, 'proc') as mock_proc:
            self.proto.handle_failure(mock_failure)

            self.assertIn(RET.RESTRAINT_PROBLEM, self.proto.extra_retcodes)
            mock_proc.signalProcess.assert_called_with('TERM')

            mock_error.assert_called_with("Fatal exception caught %s", mock_failure.getTraceback())

        with mock.patch.object(self.proto, 'proc') as mock_proc:
            mock_proc.signalProcess.side_effect = itertools.chain([ProcessExitedAlready()],
                                                                  itertools.cycle([None]))

            self.proto.handle_failure(mock_failure)

    @mock.patch('builtins.print')
    def test_check_stderr(self, mock_print):
        """Ensure _check_stderr detects EWD hit."""
        self.proto._check_stderr('Recipe 1234 exceeded lab watchdog timer')

        self.assertIn(1234, self.proto.recipe_ids_dead)
        mock_print.assert_called_with('*** Recipe 1234 hit lab watchdog!')

    @mock.patch('restraint_wrap.restraint_protocol.reactor')
    @mock.patch('upt.logger.LOGGER.error')
    def test_check_stderr_fatal(self, mock_error, mock_reactor):
        """Ensure _check_stderr works."""
        err_indication = 'Could not resolve hostname'
        mock_reactor.running = True
        self.proto._check_stderr(err_indication)
        mock_error.assert_called_with("Exiting because: %s", err_indication)
        self.assertIn(RET.RESTRAINT_PROBLEM, self.proto.extra_retcodes)
        mock_reactor.callFromThread.assert_called()

    @mock.patch('restraint_wrap.restraint_protocol.reactor')
    @mock.patch('upt.logger.LOGGER.error', mock.Mock())
    def test_check_stderr_fatal_no_reactor(self, mock_reactor):
        """Ensure _check_stderr doesn't stop reactor when it's not running."""
        err_indication = 'Could not resolve hostname'
        mock_reactor.running = False
        self.proto._check_stderr(err_indication)
        mock_reactor.callFromThread.assert_not_called()

    @mock.patch('restraint_wrap.restraint_protocol.reactor')
    @mock.patch('upt.logger.LOGGER.error', mock.Mock())
    def test_check_stderr_fatal_exc_handled(self, mock_reactor):
        """Ensure _check_stderr handles reactor exception."""
        err_indication = 'Could not resolve hostname'
        mock_reactor.running = True
        mock_reactor.callFromThread.side_effect = itertools.chain([ReactorNotRunning()], itertools.cycle([None]))

        self.proto._check_stderr(err_indication)
        mock_reactor.callFromThread.assert_called()

    @mock.patch('upt.logger.LOGGER.debug')
    @mock.patch('restraint_wrap.restraint_protocol.defer')
    def test_err_received(self, mock_defer, mock_debug):
        """Ensure errReceived works."""
        self.proto.errReceived(b'some error')
        mock_debug.assert_called_with('some error')
        mock_defer.Deferred.assert_called()

        self.proto.errReceived(b'')

    @mock.patch('restraint_wrap.restraint_protocol.RestraintClientProcessProtocol.identify_host', lambda *a: False)
    @mock.patch('restraint_wrap.restraint_protocol.reactor')
    def test_filter_output_with_path(self, mock_reactor):
        """Ensure filter_output works."""
        result = self.proto.filter_output('Using ./job.01 for job run')

        path2watch = os.path.join(f'{os.path.abspath(self.proto.kwargs["output"])}',
                                  os.path.relpath(self.proto.run_path_suffix))

        mock_reactor.callInThread.assert_called_with(self.proto.watcher.run, path2watch)
        self.assertIsNone(result)

    @mock.patch('restraint_wrap.restraint_protocol.RestraintClientProcessProtocol.identify_host', lambda *a: False)
    @mock.patch('restraint_wrap.restraint_protocol.reactor', mock.Mock())
    def test_filter_output_host_line(self):
        """Ensure filter_output process host lines."""
        line = '[aaaa-bbbbbb-01.ccccc] T:       1 [test1             ] Running'
        result = self.proto.filter_output(line)

        self.assertEqual(result, [('aaaa-bbbbbb-01.ccccc', 'T:       1 [test1             ] Running')])

        self.assertIsNone(self.proto.filter_output('err'))

    @mock.patch('builtins.print')
    def test_find_host_lines(self, mock_print):
        """Ensure find_host_lines works."""
        # Create new protocol without watcher mocked.
        proto = RestraintClientProcessProtocol(self.beaker, self.beaker.rgs[0], lambda *args: None,
                                               **self.std_kwargs)

        self.assertIsNone(proto.find_host_lines('blah'))
        proto.rst_hosts.append(RestraintHost('aaaa-bbbbbb-01.ccccc', 123))
        line = '[aaaa-bbbbbb-01.ccccc] T:       1 [test1             ] Running'
        proto.find_host_lines(line)
        mock_print.assert_called_with('Recipe: #123: Host aaaa-bbbbbb-01.ccccc: Running: test1')

    @mock.patch('upt.logger.LOGGER.info')
    def test_find_host_lines_fail(self, mock_info):
        """Ensure find_host_lines warns on suspicious output."""
        self.proto.rst_hosts.append(RestraintHost('hh', 1234))
        line = '[aa] T:       1 [test1             ] Running'
        self.proto.find_host_lines(line)
        mock_info.assert_any_call('* line: "%s"', 'T:       1 [test1             ] Running')
        mock_info.assert_any_call('!!! Received line from unknown host %s', 'aa')

    def test_split_hostline_match_invalid(self):
        """Ensure split_hostline_match handles invalid data."""
        result = self.proto.split_hostline_match('hostname1', '[hostname1] T:       1a [test1 ] Running: PASS')

        self.assertIsNone(result)

    @mock.patch('upt.logger.LOGGER.debug')
    def test_cleanup_handler_no_repeat(self, mock_debug):
        """Ensure cleanup_handler works and isn't called twice."""
        with mock.patch.object(self.proto, 'cleanup_done', True):
            self.proto.cleanup_handler()
            mock_debug.assert_not_called()

    @mock.patch('upt.logger.LOGGER.debug')
    @mock.patch('restraint_wrap.restraint_protocol.ConsoleProtocol')
    @mock.patch('restraint_wrap.restraint_protocol.Watcher')
    def test_cleanup_handler(self, mock_watcher, mock_console, mock_debug):
        """Ensure cleanup_handler works."""
        kwargs = {'keycheck': 'no', 'dev_mode': True, 'reruns': 3, 'output': '/tmp/a'}
        proto = RestraintClientProcessProtocol(self.beaker, self.beaker.rgs[0], lambda *args: None, **kwargs)
        proto.consoles.append(mock_console())
        proto.cleanup_handler()

        for host in proto.resource_group.recipeset.hosts:
            self.assertTrue(host._done_processing)

        for console in proto.consoles:
            console.save_and_terminate.assert_called()

        mock_watcher.return_value.observer.stop.assert_called()
        mock_debug.assert_called_with('restraint protocol cleanup runs (rid: %s)...', proto.resource_group.resource_id)

        self.assertTrue(proto.resource_group.recipeset._tests_finished)
        self.assertTrue(proto.cleanup_done)

    @mock.patch('upt.logger.LOGGER.debug', mock.Mock())
    @mock.patch('restraint_wrap.restraint_protocol.ConsoleProtocol', mock.Mock())
    @mock.patch('restraint_wrap.restraint_protocol.Watcher', mock.Mock())
    def test_cleanup_handler_rerun(self):
        """Ensure cleanup_handler handles reruns and adds protocols."""
        proto = RestraintClientProcessProtocol(self.beaker, self.beaker.rgs[0], lambda *args: None,
                                               **self.std_kwargs)

        proto.resource_group.recipeset._waiting_for_rerun = True

        with mock.patch.object(proto, 'add_protocol') as mock_add_protocol:
            proto.cleanup_handler()

            mock_add_protocol.assert_called()

        self.assertTrue(proto.resource_group.recipeset._tests_finished)
        self.assertTrue(proto.cleanup_done)

    @mock.patch('upt.logger.LOGGER.info')
    def test_process_ended(self, mock_info):
        """Ensure processEnded works."""
        with mock.patch.object(self.proto, 'cleanup_handler') as mock_cleanup_handler:
            reason = mock.MagicMock()
            self.proto.processEnded(reason)
            mock_cleanup_handler.assert_called()
            recipes = ['R:123', 'R:456', 'R:789']
            mock_info.assert_called_with('* restraint protocol (%s) %s retcode: %s reason: %s', recipes, 'ended.',
                                         reason.value.exitCode, reason.value)

    @mock.patch('upt.logger.LOGGER.info')
    def test_rerun_from_task(self, mock_info):
        """Ensure rerun_from_task works."""
        proto = RestraintClientProcessProtocol(self.beaker, self.beaker.rgs[0], lambda *args: None,
                                               **self.std_kwargs)

        task = proto.wrap.run_soup.findAll('task')[0]
        task_result = TaskResult(1234, task, 1, 'Pass', 'Completed')
        recipeset = self.beaker.rgs[0].recipeset
        host = recipeset.hosts[0]

        rerun_kwargs = {'host': host, 'task_result': task_result, 'task_id': 1}
        proto.rerun_from_task(**rerun_kwargs)

        self.assertEqual(host._rerun_recipe_tasks_from_index, 1)
        self.assertEqual(recipeset._attempts_made, 1)

        msg = 'Recipe: #%i: Task %s (T:%s): Making a re-run attempt %i/%i for the entire recipeSet'
        mock_info.assert_called_with(msg, task_result.recipe_id, task_result.testname, task_result.task_id,
                                     recipeset._attempts_made, proto.kwargs['reruns'])

    @mock.patch('upt.logger.LOGGER.info')
    def test_rerun_from_task_not_called(self, mock_info):
        """Ensure rerun_from_task doesn't do anything when the host is already waiting for a rerun."""
        proto = RestraintClientProcessProtocol(self.beaker, self.beaker.rgs[0], lambda *args: None,
                                               **self.std_kwargs)

        host = self.beaker.rgs[0].recipeset.hosts[0]
        with mock.patch.object(self.beaker.rgs[0].recipeset, '_waiting_for_rerun', True):
            proto.rerun_from_task(**{'host': host})
            mock_info.assert_not_called()

    def test_add_task_result(self):
        """Ensure add_task_result works."""
        proto = RestraintClientProcessProtocol(self.beaker, self.beaker.rgs[0], lambda *args: None,
                                               **self.std_kwargs)
        host = proto.resource_group.recipeset.hosts[0]
        task = proto.wrap.run_soup.findAll('task')[0]

        # Add a matching subtassk result that must link.
        proto.unprocessed_subtask_results.add((host.recipe_id, 1, 'Test1', 'Pass', '100'))
        # Add a matching subtassk result that must not link.
        subtask_result_no_match = (1000, 1, 'Test1', 'Pass', '100')
        proto.unprocessed_subtask_results.add(subtask_result_no_match)

        result = 'Pass'
        status = 'Completed'
        kwargs = {'host': host, 'recipe_id': host.recipe_id, 'task': task, 'task_id': 1, 'result': result,
                  'status': status, 'prev_task': None}

        proto.add_task_result(**kwargs)

        self.assertEqual(proto.unprocessed_subtask_results, {subtask_result_no_match})

    @mock.patch('builtins.print')
    def test_mark_recipe_done(self, mock_print):
        """Ensure mark_recipe_done works."""
        proto = copy.deepcopy(self.proto)
        task_result = TaskResult(123, proto.wrap.run_soup.findAll('task')[0], 1, 'Pass', 'Completed')

        host = proto.resource_group.recipeset.hosts[0]
        with mock.patch.object(host, '_done_processing', True):
            proto.mark_recipe_done(**{'recipe_id': 123, 'task_result': task_result})
            mock_print.assert_not_called()
        with mock.patch.object(host, '_done_processing', False):
            proto.mark_recipe_done(**{'recipe_id': 123, 'task_result': task_result})
            mock_print.assert_called_with('Recipe: #123 is done processing [mark_recipe_done]')

    def test_mark_recipe_done_raises(self):
        """Ensure mark_recipe_done raises error on invalid data."""
        proto = copy.deepcopy(self.proto)
        task_result = TaskResult(1234, proto.wrap.run_soup.findAll('task')[0], 1, 'Pass', 'Completed')

        with self.assertRaises(RuntimeError):
            proto.mark_recipe_done(**{'recipe_id': 5678, 'task_result': task_result})

    @mock.patch('upt.logger.LOGGER.debug', mock.Mock())
    def test_is_ewd_hit(self):
        """Ensure is_ewd_hit works."""
        with mock.patch.object(self.proto, 'recipe_ids_dead', {1234}):
            self.assertTrue(self.proto.is_ewd_hit(1234, 'Aborted'))

        self.assertFalse(self.proto.is_ewd_hit(1234, 'Aborted'))

    def test_mark_lwd_hit(self):
        """Ensure mark_lwd_hit works."""
        proto = RestraintClientProcessProtocol(self.beaker, self.beaker.rgs[0], lambda *args: None,
                                               **self.std_kwargs)
        proto.unprocessed_lwd_hits.add((1234, 1))

        task_result = TaskResult(1234, proto.wrap.run_soup.findAll('task')[0], 1, 'Pass', 'Completed')
        proto.mark_lwd_hit(**{'task_result': task_result})

        self.assertEqual(proto.unprocessed_lwd_hits, set())

    def test_prepare_actions(self):
        """Ensure prepare_actions works."""
        for item in self.proto._prepare_actions():
            self.assertIsInstance(item, ActionOnResult)

    def test_is_lwd_hit(self):
        """Ensure is_lwd_hit works."""
        proto = copy.deepcopy(self.proto)

        with mock.patch.object(proto.watcher, 'get_lwd_hits', lambda: {(123, 1), (123, 2)}):
            self.assertTrue(proto.is_lwd_hit(123, 1))
            self.assertTrue(proto.is_lwd_hit(123, 2))
            self.assertFalse(proto.is_lwd_hit(123, 200))

    def test_is_last_host_task(self):
        """Ensure is_last_host_task works."""
        proto = RestraintClientProcessProtocol(self.beaker, copy.deepcopy(self.beaker.rgs[0]), lambda *args: None,
                                               **self.std_kwargs)
        copied_recipeset = copy.deepcopy(proto.resource_group.recipeset)

        self.assertTrue(proto.is_last_host_task(123, 2))

        with self.assertRaises(AssertionError):
            proto.resource_group.recipeset = copied_recipeset
            self.assertFalse(proto.is_last_host_task(756, 200))

    @mock.patch('builtins.print')
    def test_evaluate_task_result(self, mock_print):
        """Ensure evaluate_task_result works."""
        resource_group = self.beaker.rgs[0]
        host0 = resource_group.recipeset.hosts[0]
        host1 = copy.deepcopy(host0)
        host1._done_processing = True
        host1.recipe_id = 456

        resource_group.recipeset.beaker_hosts = [host1, host0]
        proto = RestraintClientProcessProtocol(self.beaker, resource_group, lambda *args: None, **self.std_kwargs)
        task_result = TaskResult(123, proto.wrap.run_soup.findAll('task')[0], 1, 'Warn', 'Aborted')

        with mock.patch.object(proto.watcher, 'get_lwd_hits', lambda: {}):
            proto.evaluate_task_result(task_result, host0)
            mock_print.assert_called_with('''Recipe: #123: Aborted: Warn: abc
    (A non-waived task aborted. Re-running task to confirm infrastructure issue.)\n''')
            proto.evaluate_task_result(task_result, host1)

    @mock.patch('builtins.print')
    def test_evaluate_task_result_set_retcode(self, mock_print):
        """Ensure evaluate_task_result can call an action that sets a retcode and nothing breaks."""
        proto = RestraintClientProcessProtocol(self.beaker, self.beaker.rgs[0], lambda *args: None, **self.std_kwargs)
        task_result = TaskResult(123, self.proto.wrap.run_soup.findAll('task')[0], 1, 'Panic', 'Aborted')
        with mock.patch.object(proto.watcher, 'get_lwd_hits', lambda: {}):
            host = self.proto.resource_group.recipeset.hosts[0]
            proto.evaluate_task_result(task_result, host)
            self.assertEqual(host._retcode, RET.FAILED_TESTING)
            mock_print.assert_any_call('Recipe: #123 is done processing [mark_recipe_done]')
            mock_print.assert_any_call('''Recipe: #123: Aborted: Panic: abc -> RET.FAILED_TESTING
    (A non-waived task panicked. Kernel testing failed. Recipe processing finished.)\n''')

    @mock.patch('upt.logger.LOGGER.error')
    def test_process_task(self, mock_error):
        """Ensure process_task handles a corner case right."""
        host = self.proto.resource_group.recipeset.hosts[0]
        self.assertIsNone(self.proto.process_task(host, 999, 'Aborted', 'Panic', ''))
        mock_error.assert_called()

    def test_process_task_prev_task(self):
        """Ensure process_task loads previous task right."""
        proto = RestraintClientProcessProtocol(self.beaker, self.beaker.rgs[0], lambda *args: None, **self.std_kwargs)
        host = proto.resource_group.recipeset.hosts[0]
        task = proto.wrap.run_soup.findAll('task')[0]
        task_result = TaskResult(1234, task, 1, 'Pass', 'Completed')
        host._task_results = [task_result]

        with mock.patch.object(proto, 'add_task_result') as mock_add_task_result:
            with mock.patch.object(proto, 'evaluate_task_result'):
                with mock.patch.object(proto, 'get_restraint_xml_task') as mock_task:
                    proto.process_task(host, 1, 'Aborted', 'Panic', '')
                    mock_add_task_result.assert_called_with(recipe_id=123, task_id=1, task=mock_task.return_value,
                                                            result='Panic', status='Aborted', prev_task=task_result)
                    host._task_results = []
                    proto.process_task(host, 1, 'Aborted', 'Panic', '')
                    mock_add_task_result.assert_called_with(recipe_id=123, task_id=1, task=mock_task.return_value,
                                                            result='Panic', status='Aborted', prev_task=None)

    def test_host_hearbeat(self):
        """Ensure host_heartbeat works."""
        task = self.proto.wrap.run_soup.findAll('task')[0]
        task_result = TaskResult(1234, task, 1, 'Pass', 'Completed')
        with mock.patch.object(self.proto.provisioner, 'heartbeat') as mock_heartbeat:
            with mock.patch.object(self.proto, 'recipe_ids_dead', {123, 456}):
                with mock.patch.object(self.proto, 'ewd_recipe_ids_tasks', {(456, task_result), (123, task_result)}):
                    with mock.patch.object(self.proto, 'process_task'):
                        with mock.patch.object(self.proto, 'proc') as mock_proc:
                            mock_proc.signalProcess.side_effect = itertools.chain([ProcessExitedAlready()],
                                                                                  itertools.cycle([None]))
                            self.proto.host_hearbeat()
                            mock_heartbeat.assert_called()
                            mock_proc.signalProcess.assert_called_with('TERM')
