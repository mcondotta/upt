"""Test cases for provisioner format module."""
import os
import unittest

from cki_lib import misc

from provisioners.format import ProvisionData
from provisioners.format import Beaker

from tests.const import ASSETS_DIR


class TestProvisionerFormat(unittest.TestCase):
    """Test cases for executable module."""

    def setUp(self) -> None:
        self.req_asset = os.path.join(ASSETS_DIR, 'req.yaml')

        self.raw_data = {'beaker': {
            'rgs': [
                {
                    'job': None,
                    'resource_id': 'J:1234',
                    'recipeset':
                        {'restraint_xml': '<xml />',
                         'beaker_hosts': [{'hostname': 'hostname1',
                                           'recipe_id': 123,
                                           'duration': 2880,
                                           'recipe_fill':
                                               '<hostRequires force="abc" />'
                                           }]
                         }
                    }
            ]}}

    def test_derialize(self):
        """Ensure deserialization works."""
        prov_data = ProvisionData.deserialize_file(self.req_asset)
        recipeset0 = prov_data.beaker.rgs[0].recipeset
        self.assertIn('''<job> <recipeSet> <recipe id="123" ks_meta="harness='restraint-rhts beakerlib'"> '''
                      '''<task name="abc" /> <task name="/distribution/check-install" '''
                      'role="STANDALONE" /> </recipe> </recipeSet> </job>', recipeset0.restraint_xml)

        self.assertEqual('hostname1', recipeset0.hosts[0].hostname)
        self.assertEqual(123, recipeset0.hosts[0].recipe_id)
        self.assertEqual('<recipe ks_meta="re"><hostRequires /></recipe>',
                         recipeset0.hosts[0].recipe_fill)

    def test_serialize(self):
        """Ensure serialization works with dict."""
        with misc.tempfile_from_string(b'') as fname:
            ProvisionData(self.raw_data).serialize2file(fname)

    def test_provisioners(self):
        """Ensure .provisioners() works."""
        prov_data = ProvisionData.deserialize_file(self.req_asset)
        result = prov_data.provisioners()

        self.assertIsInstance(result[0], Beaker)

    def test_provisioners_raise(self):
        """Ensure each provisioner has resource groups."""
        raw_data = {'beaker': {'rgs': []}}

        with self.assertRaises(RuntimeError):
            ProvisionData(raw_data).provisioners()
