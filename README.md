# UPT

Unified Provisioning Tool

**Current status: verified PoC**

Provisioners available:
* Beaker
* AWS

## Maintainers

* [jracek](https://gitlab.com/jracek)
* 1 rack free! 

## Description

UPT is supposed to be a Python-3 project to unify all testing resource 
provisioning in CKI Pipelines. 

Beaker is quite closely tied with restraint. To run restraint, one needs to know
what tests to run, a "fake recipe id" and a hostname. The first two are
no issue. The fake recipe id is used to determine what host (identified by
this recipe id) will run what tests. However, with Beaker, we don't necessarily
know right away what hostname will be provisioned. Because of this, 
UPT waits until the resource is provisioned and only then it can
output restraint commands to run tests.   

Currently, the project is PoC and a tool to run tests on a provisioned resource(s)
using restraint in standalone mode is also part of this project (but is kept in
separate package). We realize it may need to be moved away. The design of the
modules is made with that in mind.

## Installation & dependencies

* The project needs `bkr` and `restraint` executables. Install the packages by:
`$ yum install -y beaker-client restraint`

* Install Python libraries:
`$ pip3 install --user -r requirements.txt`

* Install UPT:
`$ cd upt && pip3 install --user --editable .`

* (`upt` and `rcclient` links are created automatically)

## Provisioners status:

* Beaker (kpet legacy mode): 
    * full support: consumes `kpet` output to provide provisioning and test running
* Beaker (normal provisioning)
    * basic provisioning functionality is ready, but not integrated to CLI
* AWS: to be imported from other people's scripts
* (no other provisioners yet)

## Usage and provisioner format

### Basic usage (kpet legacy mode)

* Drop `beaker.xml` file to `upt` directory.
* Run command exactly as written here: `upt legacy provison` 

This will provision resources according to `beaker.xml` and prepare `run`
directory for restraint test runner to run tests.

This will also create `c_req.yaml` (converted provisioning request).
This request both describes what was provisioned and how to provision similar
resource again. The provisoning done by `c_req.yaml` will provision the same
was as if `beaker.xml` was used. The difference is that `c_req.yaml` splits
what tasks are used to provision the resource and what are used to run tests.
`c_req.yaml` also contains hostnames that we received after the provisioning
was finished.

* Afterwards, to run tests on provisioned resources using restraint and report results:
    * `$ rcclient test`

### Basic usage (non-legacy mode)

* Create `req.yaml` (as in provisioning request)
  * This describes what resource(s) you want to provision and what tests to run.

* To provision resources and wait until ready:
    * `$ upt legacy provison`
    * `$ upt provision`
* Afterwards, to run tests on provisioned resources using restraint and report results:
    * `$ rcclient test`

### Basic usage explained + provisioner format

The provisioner file format is described below.
It is a PoC and is a subject of change.

Below we see the yaml file content. 

Here are some basic rules:
* Each `recipe_fill` explains how to provision one (1) host.
* Each `restraint_xml` explains what tests to run for one (1) recipeSet, possibly with multiple recipes.
* The recipes/hosts in a recipeSet are ordered in a natural way. 
    * This means the 1st provisioned recipe matches 1st recipe `restraint_xml`
* `recipeid` / `hostname` fields explicitly tell what tests to run on what host. See `id` attribute of a recipe in `resraint_xml`.
* Beaker only: `duration` specifies for how long (in seconds) will the host stay provisioned. Each time we start running
tests on the host, we renew this duration, because we certainly don't want the machine to disappear in the middle of testing.

Once `$ upt provision` is run, it will do following:
* provision hosts according to `recipe_fill`. The order of resources (hosts)
* The provisioner will output files in output directory (default directory: `run`) 
  * These files are `restraint` commands to actually run tests on the provisioned hosts.
  * There are also files to provide ssh aliases and allow autossh tunneling.
```
!<ProvisionData>
beaker: !<Beaker>
  rgs:
    - !<ResourceGroup>
      job:
      resource_id: J:1234
      recipeset: !<RecipeSet>
          restraint_xml:
          <?xml version="1.0" encoding="utf-8"?>
          <job>
           <recipeSet>
            <recipe id="123" role="STANDALONE">
              <task name="name of my great test" role="STANDALONE">
                <fetch url="git://from_where_to_fetch_it"/>
                <params>
                  <param name="CKI_NAME" value="name of my great test"/>
                  <param name="CKI_MAINTAINERS" value="..."/>
                  <param name="KILLTIMEOVERRIDE" value="3600"/>
                </params>
              </task>
            </recipe>
           </recipeSet>
          </job>
          beaker_hosts:
            - !<Host>
              hostname: host_to_run_on
              recipe_id: 123
              duration: 2880
              recipe_fill: |-
                <recipe> ... </recipe>
```
The `!<text>` labels are yaml labels for Python classes,
so yaml library automatically knows how to deserialize into objects.

Running `upt --help` or `rcclient --help` is your friend.
The parameters should be reasonably documented there. If not, then let's
fix that.

### AWS provisioner example

To use AWS provisioner, you need to have an AWS account and your system has to be configured to use it.
This usually includes setting AWS access key id, AWS secret access key and AWS region in config file. 
Refer to https://docs.aws.amazon.com/sdk-for-java/v1/developer-guide/setup-credentials.html for more info.

To be able to provision an instance, a launch template has to exist. The environment variable `AWS_UPT_CONFIG`
is used to specify which template to use.

The example below configures UPT to use a default launch template, which can be created using `upt aws-setup` command.
**NOTE: please use aws-setup command only with a private account, to make sure you don't destroy important settings.**

```
export AWS_UPT_CONFIG='{"launch_template": "upt.default.launch-template", "instance_prefix": "default.staging.i.upt"}'
```

Otherwise, using AWS provisioner is very similar to using Beaker. The biggest difference is that the node is named
`aws` and hosts are inside `aws_hosts` node.

When an instance is provisioned, `misc` field and `instance_id` is created in `Host` node.
This allows UPT to use existing running instances using `upt waiton` command, instead of provisioning them. 
This is useful especially during development; you don't have to keep launching and terminating instances.

Notes:
* Currently, the provisioner provisions only `t2.micro` instances (hardcoded).
* `duration` and `resource_id` fields are currently ignored.
* As you can see below, it is possible to provision machines from AWS and Beaker at the same time.

```
!<ProvisionData>
aws: !<AWS>
  rgs:
    - !<ResourceGroup>
      resource_id: '1'
      recipeset: !<RecipeSet>
          aws_hosts:
            - !<Host>
              hostname: hostname
              recipe_id: 1
              recipe_fill:
              duration: 28800
              misc:
                instance_id: i-00000000000000001
          restraint_xml: |-
            <?xml version="1.0" encoding="utf-8"?>
            <job>
             <recipeSet>
              <recipe id="1">
               <task name="testtask" role="STANDALONE">
                <fetch url="https://gitlab.com/jracek/testtask/-/archive/master/testtask-master.zip#abort"/>
                <params>
               </task>
              </recipe>
             </recipeSet>
            </job>
          beaker_hosts: []
      job: <job group retention_tag="60days"><whiteboard>UPT@gitlab:123456 3.18@rhel
        x86_64</whiteboard></job>
      preprovisioned: false
beaker: !<Beaker>
  rgs: []
```

### Notes

Currently, machine has 15 minutes to reboot. If it's longer, it's considered an issue.

### Even more info

See `docs` directory, it contains useful info about some design decisions and explains why UPT works the way it does.
